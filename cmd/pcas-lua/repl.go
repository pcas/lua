// Repl runs the session in interactive mode.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/repl"
	"context"
	"fmt"
	"io"
	"runtime"
	"strings"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// printWelcome prints the default usage message to REPL.
func printWelcome(r *repl.Repl) {
	// Is there anything to do?
	if !r.IsTerminal() {
		return
	}
	// Create the usage message
	p := r.Printer()
	text := p.WrapString("Type .help for help. Type .exit or press ^D to exit.")
	for _, s := range []string{
		".help",
		".exit",
	} {
		text = strings.Replace(text, s, p.Bold(s), -1)
	}
	// Output the usage message
	fmt.Fprintln(r.Stdout(), text)
}

// printGoodbye prints the default goodbye message to the REPL, providing info about the memory usage and total runtime.
func printGoodbye(r *repl.Repl) {
	// Is there anything to do?
	if !r.IsTerminal() {
		return
	}
	// Fetch the memory usage
	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)
	m := float64(mem.HeapAlloc) / (1 << 20) // TODO: Which memory stat is best?
	// Fetch the runtime
	t := r.Runtime().Truncate(time.Millisecond)
	// Output the stats
	text := fmt.Sprintf("Total time: %s, Total memory usage: %.2fMB", t, m)
	fmt.Fprintln(r.Stdout(), r.Printer().WrapString(text))
}

// runInteractive runs Lua in interactive mode. It creates and runs a REPL wrapping vm and writing to w.
func runInteractive(vm *rt.Runtime, w io.Writer, opts *Options) error {
	// Create the REPL
	r, err := repl.NewREPL(vm, w)
	if err != nil {
		return err
	}
	defer r.Close()
	// Should we disable colourised output?
	if opts.NoColor {
		r.SetColor(false)
	}
	// Output the welcome message and defer the goodbye message
	if !opts.NoBanner {
		printWelcome(r)
		defer printGoodbye(r)
	}
	// Load and execute the rc file, if any
	if len(opts.Rc) != 0 {
		if !opts.NoBanner {
			fmt.Printf("Loading startup file \"%s\"\n", opts.Rc)
		}
		if err := loadAndExecFile(vm, opts.Rc); err != nil {
			r.PrintlnError(err) // Ignore any error
		}
	}
	// Run the REPL
	for err == nil {
		err = r.Next(context.Background())
	}
	// Clear an io.EOF before returning
	if err == io.EOF {
		err = nil
	}
	return err
}
