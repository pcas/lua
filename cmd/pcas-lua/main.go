// Pcaslua implements a Lua REPL.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua"
	"bitbucket.org/pcas/lua/lib/osutillib"
	"bitbucket.org/pcastools/cleanup"
	"context"
	"fmt"
	"os"
	"runtime"
	"strings"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		// Create the error message
		text := rt.Traceback(err)
		// Output the error message to stderr
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, strings.TrimSpace(text))
		os.Exit(1)
	}
}

// setLuaDefaults sets the default values in the lua libraries based on the given options.
func setLuaDefaults(opts *Options) {
	// Set the args
	osutillib.Args = opts.Args
}

// runFinalisers runs GC in order to give any finalisers set by Lua a change to execute before we exit.
func runFinalisers() {
	runtime.GC()
	runtime.GC()
	runtime.GC()
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running the cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse and set the options
	opts := setOptions()
	setLuaDefaults(opts)
	// Give lua's finalizers a chance to run
	defer runFinalisers()
	// Run lua
	err = func() (err error) {
		// Recover from any panics
		defer func() {
			if e := recover(); e != nil {
				if err == nil {
					err = fmt.Errorf("panic: %v", e)
				}
			}
		}()
		// Create a new VM
		var r *rt.Runtime
		if r, err = lua.New(os.Stdout); err != nil {
			return
		}
		// Are we to run in interactive or non-interactive mode?
		if len(opts.Args) == 0 {
			err = runInteractive(r, os.Stdout, opts)
		} else {
			err = runNonInteractive(r, opts)
		}
		return
	}()
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
