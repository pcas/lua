// Flags.go defines two command-line flag types that are used when parsing command-line flags in config.go

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/flag"
)

/////////////////////////////////////////////////////////////////////////
// boolFlagNoDefault functions
/////////////////////////////////////////////////////////////////////////

// bf implements Flag and defines a boolean-valued flag.  Its description does not display a default value.
type bf struct {
	name        string // The name of the flag.
	description string // The short-form help text for this flag.
	usage       string // The long-form help text for this flag.
	b           *bool  // A pointer to the variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *bf) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *bf) Description() string {
	return f.description
}

// Usage returns long-form usage text (or the empty string if no such usage text is set).
func (f *bf) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *bf) Parse(in string) error {
	val, err := convert.ToBool(in)
	if err != nil {
		return err
	}
	*(f.b) = val
	return nil
}

// IsBoolean indicates that this flag is a boolean flag in the sense of the flag package.
func (f *bf) IsBoolean() bool {
	return true
}

// boolFlagNoDefault returns a Flag that represents a boolean flag with the given name, description, usage string, backing variable, and default value.
func boolFlagNoDefault(name string, b *bool, def bool, description string, usage string) flag.Flag {
	result := &bf{
		name:        name,
		description: description,
		usage:       usage,
		b:           b,
	}
	*(result.b) = def
	return result
}

/////////////////////////////////////////////////////////////////////////
// stringFlagNoDefault functions
/////////////////////////////////////////////////////////////////////////

// sf implements Flag and defines a string-valued flag.  Its description does not display a default value.
type sf struct {
	name        string  // The name of the flag.
	description string  // The short-form help text for this flag.
	usage       string  // The long-form help text for this flag.
	s           *string // The variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *sf) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *sf) Description() string {
	return f.description
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *sf) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *sf) Parse(in string) error {
	*(f.s) = in
	return nil
}

// stringFlagNoDefault returns a Flag that represents a string-valued flag with the given name, description, and usage string.  It has backing variable s, and default value def.  Its description does not display a default value.
func stringFlagNoDefault(name string, s *string, def string, description string, usage string) flag.Flag {
	result := &sf{
		name:        name,
		description: description,
		usage:       usage,
		s:           s,
	}
	*(result.s) = def
	return result
}
