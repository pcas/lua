// Logginglib contains Lua glue for the logd package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logginglib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/lib/packagelib"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/lua/luautil"
	"bitbucket.org/pcastools/address"
	"time"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	timeout   time.Duration // The default timeout
	metatable *rt.Table     // The metatable for a logger
}

// config describes the configuration.
type config struct {
	*logd.ClientConfig        // The logd client configuration
	identifier         string // The identifier
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// configToTable returns a table representing the given config.
func configToTable(cfg *config) *rt.Table {
	t := rt.NewTable()
	luautil.SetString(t, "address", cfg.URI())
	luautil.SetString(t, "sslcert", string(cfg.SSLCert))
	luautil.SetString(t, "identifier", cfg.identifier)
	return t
}

// tableToConfig returns a copy of the given config modified with the values in t.
func tableToConfig(cfg *config, t *rt.Table) (*config, error) {
	// Move to a copy
	if cfg == nil {
		cfg = newDefaultConfig()
	} else {
		cfg = cfg.Copy()
	}
	// Modify the values
	if s, ok, err := luautil.GetString(t, "address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	if s, ok, err := luautil.GetString(t, "sslcert"); ok {
		if err != nil {
			return nil, err
		}
		cfg.SSLCert = []byte(s)
	}
	// Read in the identifier
	if identifier, ok, err := luautil.GetString(t, "identifier"); ok {
		if err != nil {
			return nil, err
		}
		cfg.identifier = identifier
	}
	// Return the config settings
	return cfg, nil
}

// defaultConfig returns the default config as specified by logging.defaults.
func defaultConfig(t *rt.Thread) (*config, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("logging"))
	if err != nil {
		return nil, err
	}
	// Recover the defaults table
	defaults, err := luautil.Subtable(pkg, "defaults")
	if err != nil {
		return nil, err
	}
	// Convert the values
	return tableToConfig(nil, defaults)
}

/////////////////////////////////////////////////////////////////////////
// config functions
/////////////////////////////////////////////////////////////////////////

// newDefaultConfig returns a copy of the default configuration prior to modification by any user-defined lua settings.
func newDefaultConfig() *config {
	return &config{
		ClientConfig: logd.DefaultConfig(),
		identifier:   logdflag.DefaultIdentifier(),
	}
}

// Copy returns a copy of the configuration.
func (cfg *config) Copy() *config {
	return &config{
		ClientConfig: cfg.ClientConfig.Copy(),
		identifier:   cfg.identifier,
	}
}

/////////////////////////////////////////////////////////////////////////
// registryData functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// SetTimeout sets the default timeout.
func (r *registryData) SetTimeout(timeout time.Duration) {
	r.timeout = timeout
}

// GetTimeout returns the default timeout.
func (r *registryData) GetTimeout() time.Duration {
	return r.timeout
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	// Register this module
	module.Register(module.Info{
		Name:        "logging",
		Description: "Provide logging functionality.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				timeout:   iobindings.NilTimeout,
				metatable: createLoggerMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnv(pkg, "defaults", configToTable(newDefaultConfig()))
			rt.SetEnvGoFunc(pkg, "connect", connect, 2, false)
			rt.SetEnvGoFunc(pkg, "settimeout", settimeout, 1, false)
			rt.SetEnvGoFunc(pkg, "gettimeout", gettimeout, 0, false)
			return pkg, nil
		},
	})
}

// connect returns a new logger connection to the logd server. If config is nil then the default configuration settings will be used. The optional table config can be used to modify these defaults; in particular, config can be used to specify values for:
//	'address'		a string specifying the address to connect to;
//	'identifier'	a string specifying the identifier to use;
//	'sslcert'		the SSL certificate (if any).
//
// Args: logName [config]
func connect(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// The user has to provide the log name
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	logName, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	// Read the config
	cfg, err := defaultConfig(t)
	if err != nil {
		return nil, err
	}
	if c.NArgs() > 1 {
		v, err := c.TableArg(1)
		if err != nil {
			return nil, err
		}
		if cfg, err = tableToConfig(cfg, v); err != nil {
			return nil, err
		}
	}
	// Create the connection
	l, err := connectToLogdServer(t, string(logName), cfg)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	return c.PushingNext(rt.NewUserData(l, getRegistryData(t).metatable)), nil
}

// settimeout sets the default timeout.
func settimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.SetTimeoutWithSetTimeouter(getRegistryData(t), c)
}

// gettimeout returns the default I/O timeout, in seconds.
func gettimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.GetTimeoutWithGetTimeouter(getRegistryData(t), c)
}
