// Tableutillib implements simple functions to manipulate Lua tables.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package tableutillib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/repl"
	"bitbucket.org/pcastools/stringsbuilder"
	"sort"
)

// valueSorter represents a sortable slice of values.
type valueSorter struct {
	t *rt.Thread
	S []rt.Value
}

/////////////////////////////////////////////////////////////////////////
// valueSorter functions
/////////////////////////////////////////////////////////////////////////

func typeToInt(v rt.Value) int {
	switch v.(type) {
	case rt.Bool:
		return 0
	case rt.Int:
		return 1
	case rt.Float:
		return 2
	case rt.String:
		return 3
	case *rt.Table:
		return 4
	default:
		return 5
	}
}

// Less reports whether the element with index i should sort before the element with index j.
func (s *valueSorter) Less(i int, j int) bool {
	v1, v2 := s.S[i], s.S[j]
	cmp, err := rt.Lt(s.t, v1, v2)
	if err == nil {
		return cmp
	}
	return typeToInt(v1) < typeToInt(v2)
}

// Swap swaps the elements with indexes i and j.
func (s *valueSorter) Swap(i int, j int) {
	s.S[i], s.S[j] = s.S[j], s.S[i]
}

// Len is the number of elements in the collection.
func (s *valueSorter) Len() int {
	return len(s.S)
}

// Sort sorts the collection.
func (s *valueSorter) Sort() {
	sort.Sort(s)
}

// ToTable returns the collection as a table.
func (s *valueSorter) ToTable() *rt.Table {
	tab := rt.NewTable()
	for i, v := range s.S {
		tab.Set(rt.Int(i+1), v)
	}
	return tab
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "tableutil",
		Description: "Implements simple functions to manipulate tables.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "count", count, 2, false)
			rt.SetEnvGoFunc(pkg, "keys", keys, 1, false)
			rt.SetEnvGoFunc(pkg, "values", values, 1, false)
			rt.SetEnvGoFunc(pkg, "uniquevalues", uniquevalues, 1, false)
			rt.SetEnvGoFunc(pkg, "tostring", tostring, 1, false)
			return pkg, nil
		},
	})
}

// count returns the number of occurrences of the item v in the table t.
// Args: t, v
func count(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	tab, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	v := c.Arg(1)
	// Start counting
	var n int
	k, x, _ := tab.Next(nil)
	for k != nil {
		// Is this a match?
		if x == v {
			n++
		}
		// Move on
		k, x, _ = tab.Next(k)
	}
	return c.PushingNext(rt.Int(n)), nil
}

// keys returns the keys of the table t.
// Args: t
func keys(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	tab, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	// Extract the keys
	S := make([]rt.Value, 0)
	k, _, _ := tab.Next(nil)
	for k != nil {
		S = append(S, k)
		k, _, _ = tab.Next(k)
	}
	// Sort the keys
	s := &valueSorter{
		t: t,
		S: S,
	}
	s.Sort()
	// Convert the keys to a table and return
	return c.PushingNext(s.ToTable()), nil
}

// values returns the values of the table t.
// Args: t
func values(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	tab, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	// Extract the values
	S := make([]rt.Value, 0)
	k, v, _ := tab.Next(nil)
	for k != nil {
		S = append(S, v)
		k, v, _ = tab.Next(k)
	}
	// Sort the values
	s := &valueSorter{
		t: t,
		S: S,
	}
	s.Sort()
	// Convert the values to a table and return
	return c.PushingNext(s.ToTable()), nil
}

// uniquevalues returns the unique values of the table t.
// Args: t
func uniquevalues(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	tab, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	// Create a map of unique values
	m := make(map[rt.Value]bool)
	k, v, _ := tab.Next(nil)
	for k != nil {
		m[v] = true
		k, v, _ = tab.Next(k)
	}
	// Convert the map to a slice and sort it
	S := make([]rt.Value, 0, len(m))
	for k := range m {
		S = append(S, k)
	}
	s := &valueSorter{
		t: t,
		S: S,
	}
	s.Sort()
	// Convert the slice to a table and return
	return c.PushingNext(s.ToTable()), nil
}

// tostring returns a string description of the table t.
// Args: t
func tostring(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	tab, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	// Create a new strings builder
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	// Print the table to the strings builder
	p := repl.NewPrinter(t.Runtime)
	p.SetScreenWidth(0)
	p.SetColor(false)
	_, err = p.FprintValueInline(b, tab)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(b.String())), nil
}
