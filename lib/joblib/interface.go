// Interface defines the interfaces that tasks and job manifests satisfy.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package joblib

import (
	"bitbucket.org/pcastools/ulid"
	"time"
)

// Task represents a task in a job scheduling system
type Task interface {
	// Name is the name of the job to which this task belongs.
	Name() string
	// JobID is the ID of the job.
	JobID() ulid.ULID
	// Manifest records the files and directories uploaded with the job.
	Manifest() Manifest
	// Script is the file in Manifest that is run for each task in the job.
	Script() string
	// WorkingDir is a directory in Manifest, either explicitly or implicitly (as a subpath).
	WorkingDir() string
	// Metadata records user-provided metadata.
	Metadata() map[string]string
	// Priority is the priority level of the job.
	Priority() uint8
	// ID is the ID of the task.
	ID() ulid.ULID
	// Value is the value of the task.
	Value() int64
	// Deadline is the time at which the task will go stale.
	Deadline() time.Time
	// Failures is the number of times that this task has failed.
	Failures() int
}

// Manifest records the files and directories associated with a task
type Manifest interface {
	// Files returns the files in the manifest.
	Files() []string
	// Directories returns the directories in the manifest.
	Directories() []string
	// ContainsFile returns true if and only if the manifest contains a file with the given path.
	ContainsFile(string) bool
}
