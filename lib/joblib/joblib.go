// Joblib provides Lua access to the pcas job data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package joblib

import (
	"bitbucket.org/pcas/golua/lib/oslib"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/lib/ulidlib"
	"bitbucket.org/pcas/lua/luautil"
	"bitbucket.org/pcastools/ulid"
	"sync"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	manifestMt *rt.Table // The metatable for a manifest
}

// The task data and controlling mutex.
var (
	taskm sync.Mutex
	task  Task
)

/////////////////////////////////////////////////////////////////////////
// registryData functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// wrapTaskFunc wraps the given function, turning it into a GoFunction.
func wrapTaskFunc(f func(*rt.Thread, Task) (rt.Value, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		// Acquire a lock on the task
		taskm.Lock()
		defer taskm.Unlock()
		// Call the function
		v, err := f(t, task)
		if err != nil {
			return nil, err
		}
		return c.PushingNext(v), nil
	}
}

// setTaskData sets the task data on the given table.
func setTaskData(t *rt.Thread, tab *rt.Table) {
	// Acquire a lock on the task
	taskm.Lock()
	defer taskm.Unlock()
	// Set the task data
	if task == nil {
		luautil.SetString(tab, "name", "")
		rt.SetEnv(tab, "jobid", ulidlib.ULIDToValue(t, ulid.Nil))
		luautil.SetString(tab, "script", "")
		luautil.SetString(tab, "workingdir", "")
		luautil.SetInt(tab, "priority", 0)
		rt.SetEnv(tab, "taskid", ulidlib.ULIDToValue(t, ulid.Nil))
		luautil.SetInt64(tab, "value", 0)
		luautil.SetInt(tab, "failures", 0)
	} else {
		luautil.SetString(tab, "name", task.Name())
		rt.SetEnv(tab, "jobid", ulidlib.ULIDToValue(t, task.JobID()))
		luautil.SetString(tab, "script", task.Script())
		luautil.SetString(tab, "workingdir", task.WorkingDir())
		luautil.SetInt(tab, "priority", int(task.Priority()))
		rt.SetEnv(tab, "taskid", ulidlib.ULIDToValue(t, task.ID()))
		luautil.SetInt64(tab, "value", task.Value())
		luautil.SetInt(tab, "failures", task.Failures())
	}
}

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "job",
		Description: "Access to the current job.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				manifestMt: createManifestMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			setTaskData(r.MainThread(), pkg)
			rt.SetEnvGoFunc(pkg, "metadata", wrapTaskFunc(metadata), 0, false)
			rt.SetEnvGoFunc(pkg, "manifest", wrapTaskFunc(manifest), 0, false)
			rt.SetEnvGoFunc(pkg, "deadline", wrapTaskFunc(deadline), 0, false)
			return pkg, nil
		},
		Requires: []string{"ulid"},
	})
}

// metadata returns the task metadata.
// Args:
func metadata(_ *rt.Thread, task Task) (rt.Value, error) {
	md := rt.NewTable()
	if task != nil {
		for k, v := range task.Metadata() {
			md.Set(rt.String(k), rt.String(v))
		}
	}
	return md, nil
}

// manifest returns the task manifest.
// Args:
func manifest(t *rt.Thread, task Task) (rt.Value, error) {
	var m Manifest
	if task != nil {
		m = task.Manifest()
	}
	return rt.NewUserData(m, getRegistryData(t).manifestMt), nil
}

// deadline returns the task deadline.
// Args:
func deadline(_ *rt.Thread, task Task) (rt.Value, error) {
	return oslib.TimeToTable(task.Deadline()), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SetTask sets the task data for the Lua job module. Note that the task data must be set BEFORE the module is first loaded into the Lua runtime by the user; attempting to set the task data after the module is loaded will have no affect.
func SetTask(t Task) {
	taskm.Lock()
	defer taskm.Unlock()
	task = t
}
