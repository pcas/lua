// Manifest provides Lua access to a job manifest.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package joblib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// manifestMethod wraps the GoFunction f with a check that arg(0) is a Manifest.
func manifestMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err = ManifestArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// wrapManifestArg wraps the given function, turning it into a GoFunction.
func wrapManifestArg(f func(Manifest, *rt.GoCont) (rt.Value, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		m, err := ManifestArg(c, 0)
		if err != nil {
			return nil, err
		}
		v, err := f(m, c)
		if err != nil {
			return nil, err
		}
		return c.PushingNext(v), nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createManifestMetatable creates the metatable for a job manifest.
func createManifestMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "containsfile", wrapManifestArg(containsfile), 2, false)
	rt.SetEnvGoFunc(methods, "dirs", wrapManifestArg(dirs), 1, false)
	rt.SetEnvGoFunc(methods, "files", wrapManifestArg(files), 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", manifestMethod(iobindings.ToString), 1, false)
	return meta
}

// containsfile returns true iff the manifest contains the given file path.
// Args: m path
func containsfile(m Manifest, c *rt.GoCont) (rt.Value, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	p, err := c.StringArg(1)
	if err != nil {
		return nil, err
	} else if len(p) == 0 {
		return nil, errors.New("#2 must not be the empty string")
	}
	return rt.Bool(m.ContainsFile(string(p))), nil
}

// dirs returns the array of directory paths in the manifest.
// Args: m
func dirs(m Manifest, _ *rt.GoCont) (rt.Value, error) {
	t := rt.NewTable()
	for i, p := range m.Directories() {
		t.Set(rt.Int(i+1), rt.String(p))
	}
	return t, nil
}

// files returns the array of file paths in the manifest.
// Args: m
func files(m Manifest, _ *rt.GoCont) (rt.Value, error) {
	t := rt.NewTable()
	for i, p := range m.Files() {
		t.Set(rt.Int(i+1), rt.String(p))
	}
	return t, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ManifestArg turns a continuation argument into a Manifest.
func ManifestArg(c *rt.GoCont, n int) (Manifest, error) {
	m, ok := ValueToManifest(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a job manifest", n+1)
	}
	return m, nil
}

// ValueToManifest turns a lua value to a Manifest, if possible.
func ValueToManifest(v rt.Value) (Manifest, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	m, ok := u.Value().(Manifest)
	if !ok {
		return nil, false
	}
	return m, true
}
