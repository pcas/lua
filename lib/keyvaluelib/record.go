// Record describes a Lua view of a record.Record.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvaluelib

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue/record"
	"errors"
	"fmt"
	"math"
	"strconv"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// recordValueToLua converts the given record.Record value to a lua value.
func recordValueToLua(x interface{}) rt.Value {
	switch v := x.(type) {
	case int:
		return rt.Int(v)
	case int8:
		return rt.Int(v)
	case int16:
		return rt.Int(v)
	case int32:
		return rt.Int(v)
	case int64:
		return rt.Int(v)
	case uint:
		if v > math.MaxInt64 {
			return rt.String(strconv.FormatUint(uint64(v), 10))
		}
		return rt.Int(v)
	case uint8:
		return rt.Int(v)
	case uint16:
		return rt.Int(v)
	case uint32:
		return rt.Int(v)
	case uint64:
		if v > math.MaxInt64 {
			return rt.String(strconv.FormatUint(v, 10))
		}
		return rt.Int(v)
	case bool:
		return rt.Bool(v)
	case float64:
		return rt.Float(v)
	case string:
		return rt.String(v)
	case []byte:
		return rt.String(v)
	}
	return nil
}

// luaToRecordValue attempts to convert the given lua value to a valid record.Record value. The second returned value is true on success, false otherwise.
func luaToRecordValue(x rt.Value) (interface{}, bool) {
	switch v := x.(type) {
	case rt.String:
		return string(v), true
	case rt.Int:
		return int64(v), true
	case rt.Float:
		return float64(v), true
	case rt.Bool:
		return bool(v), true
	}
	return nil, false
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// RecordToTable converts the given record.Record to a lua table.
func RecordToTable(r record.Record) *rt.Table {
	t := rt.NewTable()
	for k, v := range r {
		rt.SetEnv(t, k, recordValueToLua(v))
	}
	return t
}

// TableToRecord converts the given lua table to a record.Record.
func TableToRecord(t *rt.Table) (record.Record, error) {
	// Create an empty record
	r := record.Record{}
	// Start iterating over the table, populating the record as we go
	k, v, _ := t.Next(nil)
	for k != nil {
		// The key must be a string
		kk, ok := k.(rt.String)
		if !ok {
			return record.Record{}, errors.New("keys must be strings")
		}
		// Convert the value
		vv, ok := luaToRecordValue(v)
		if !ok {
			return record.Record{}, errors.New("unable to convert value for key '" + string(kk) + "'")
		}
		// Update the record and move on
		r[string(kk)] = vv
		k, v, _ = t.Next(k)
	}
	// Check that the record validates
	if _, err := r.IsValid(); err != nil {
		return nil, err
	}
	// Return the record
	return r, nil
}

// RecordArg turns a continuation argument into a record.Record.
func RecordArg(c *rt.GoCont, n int) (record.Record, error) {
	t, ok := c.Arg(n).(*rt.Table)
	if !ok {
		return record.Record{}, fmt.Errorf("#%d does not describe a record: value must be a table", n+1)
	}
	r, err := TableToRecord(t)
	if err != nil {
		return record.Record{}, fmt.Errorf("#%d does not describe a record: %w", n+1, err)
	}
	return r, nil
}

// ValueToRecord turns a lua value to a record.Record, if possible.
func ValueToRecord(v rt.Value) (record.Record, bool) {
	t, ok := v.(*rt.Table)
	if !ok {
		return record.Record{}, false
	}
	r, err := TableToRecord(t)
	if err != nil {
		return record.Record{}, false
	}
	return r, true
}
