// Connection describes a Lua view of a keyvalue.Connection.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvaluelib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"runtime"
	"time"
)

// Connection wraps a keyvalue.Connection.
type Connection struct {
	log.Logable
	metrics.Metricsable
	c        *keyvalue.Connection // The underlying connection
	timeout  time.Duration        // The timeout
	isClosed bool                 // Is the connection closed
	closeErr error                // The error on close (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// connectionMethod wraps the GoFunction f with a check that arg(0) is a *Connection.
func connectionMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := ConnectionArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

/////////////////////////////////////////////////////////////////////////
// Connection functions
/////////////////////////////////////////////////////////////////////////

// openConnectionAsValue opens a Connection to the given data source (using openConnection) and returns a new rt.Value representing this connection.
func openConnectionAsValue(dataSource string, t *rt.Thread) (rt.Value, error) {
	conn, err := openConnection(dataSource, t)
	if err != nil {
		return nil, err
	}
	return rt.NewUserData(conn, getRegistryData(t).connectionMt), nil
}

// openConnection opens a Connection to the given data source. A finaliser is set on the connection to ensure that it is closed.
func openConnection(dataSource string, t *rt.Thread) (*Connection, error) {
	// Recover the timeout value
	timeout := getRegistryData(t).GetTimeout()
	// Create the context
	var cancel context.CancelFunc
	ctx := context.Background()
	if timeout < 0 {
		ctx, cancel = context.WithCancel(ctx)
	} else {
		ctx, cancel = context.WithTimeout(ctx, timeout)
	}
	defer cancel()
	// Link the context to the interrupt channel
	defer contextutil.NewChannelLink(t.Interrupt(), cancel).Stop()
	// Open a connection
	c, err := keyvalue.Open(ctx, dataSource)
	if err != nil {
		return nil, err
	}
	// Create the wrapped-up connection and set the finaliser
	conn := &Connection{
		Logable:     c,
		Metricsable: c,
		c:           c,
		timeout:     timeout,
	}
	runtime.SetFinalizer(conn, func(conn *Connection) { conn.Close() })
	return conn, nil
}

// Close prevents any further queries through this connection.
func (conn *Connection) Close() error {
	if !conn.IsClosed() {
		conn.isClosed = true
		conn.closeErr = conn.c.Close()
		runtime.SetFinalizer(conn, nil)
	}
	return nil
}

// IsClosed returns true iff the connection is closed.
func (conn *Connection) IsClosed() bool {
	return conn == nil || conn.c == nil || conn.isClosed
}

// GetTimeout returns the timeout.
func (conn *Connection) GetTimeout() time.Duration {
	if conn == nil {
		return 0
	}
	return conn.timeout
}

// SetTimeout sets the timeout. A negative timeout is interpreted to mean no timeout.
func (conn *Connection) SetTimeout(timeout time.Duration) {
	if conn != nil {
		conn.timeout = timeout
	}
}

// DriverName returns the name of the associated driver.
func (conn *Connection) DriverName() string {
	if conn == nil {
		return ""
	}
	return conn.c.DriverName()
}

// DataSource returns the data source string used to establish the connection. Note that this may contain sensitive user data such as a password.
func (conn *Connection) DataSource() string {
	if conn == nil {
		return ""
	}
	return conn.c.DataSource()
}

// ListTables returns the names of the tables in the database. The names are sorted in increasing order.
func (conn *Connection) ListTables(ctx context.Context) ([]string, error) {
	// If necessary, add a timeout to the context
	timeout := conn.GetTimeout()
	if timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	// List the tables
	return conn.c.ListTables(ctx)
}

// CreateTable creates a table with the given name in the database.
//
// The provided template will be used by the underlying storage-engine to create the new table if appropriate; the storage-engine is free to ignore the template if it is not required (for example, in a schema-less database).
func (conn *Connection) CreateTable(ctx context.Context, name string, template record.Record) error {
	// If necessary, add a timeout to the context
	timeout := conn.GetTimeout()
	if timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	// Create the table
	return conn.c.CreateTable(ctx, name, template)
}

// DeleteTable deletes the indicated table from the database. Does not return an error if the table does not exist.
func (conn *Connection) DeleteTable(ctx context.Context, name string) error {
	// If necessary, add a timeout to the context
	timeout := conn.GetTimeout()
	if timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	// Delete the table
	return conn.c.DeleteTable(ctx, name)
}

// ConnectToTable connects to the indicated table in the database. A finaliser is set on the table to ensure that it is closed.
func (conn *Connection) ConnectToTable(ctx context.Context, name string) (*Table, error) {
	// If necessary, add a timeout to the context
	timeout := conn.GetTimeout()
	if timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	// Connect to the table
	t, err := conn.c.ConnectToTable(ctx, name)
	if err != nil {
		return nil, err
	}
	// Wrap the table and return
	tt := NewTable(t)
	tt.SetTimeout(timeout)
	return tt, nil
}

// String returns a string description of the connection.
func (conn *Connection) String() string {
	return "connection(\"" + conn.DriverName() + "\")"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createConnectionMetatable creates the metatable for a connection.
func createConnectionMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "settimeout", connectionMethod(iobindings.SetTimeout), 2, false)
	rt.SetEnvGoFunc(methods, "gettimeout", connectionMethod(iobindings.GetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "close", connectionMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", connectionMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "datasource", datasource, 1, false)
	rt.SetEnvGoFuncContext(methods, "listtables", listtables, 1, false)
	rt.SetEnvGoFuncContext(methods, "deletetable", deletetable, 2, false)
	rt.SetEnvGoFuncContext(methods, "createtable", createtable, 3, false)
	rt.SetEnvGoFuncContext(methods, "table", table, 2, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", connectionMethod(iobindings.ToString), 1, false)
	return meta
}

// datasource returns the data source for this connection. Note that this may contain sensitive user data such as a password.
// Args: conn
func datasource(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	conn, err := ConnectionArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(conn.DataSource())), nil
}

// listtables returns a list of all tables in the database.
// Args: conn
func listtables(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	// Recover the connection
	conn, err := ConnectionArg(c, 0)
	if err != nil {
		return nil, err
	}
	// List the tables
	S, err := conn.ListTables(ctx)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	// Convert the list to a lua object and return
	T := rt.NewTable()
	for i, s := range S {
		T.Set(rt.Int(i+1), rt.String(s))
	}
	return c.PushingNext(T), nil
}

// deletetable deletes a table from the database.
// Args: conn tablename
func deletetable(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the connection and the table name
	conn, err := ConnectionArg(c, 0)
	if err != nil {
		return nil, err
	}
	name, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	// Delete the table
	return iobindings.PushingNextResult(c, rt.Bool(true), conn.DeleteTable(ctx, string(name)))
}

// createtable creates a new table in the database using the given template.
// Args: conn tablename template
func createtable(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	// Recover the connection, table name, and template
	conn, err := ConnectionArg(c, 0)
	if err != nil {
		return nil, err
	}
	name, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	template, err := RecordArg(c, 2)
	if err != nil {
		return nil, err
	}
	// Create the table
	return iobindings.PushingNextResult(c, rt.Bool(true), conn.CreateTable(ctx, string(name), template))
}

// table returns a connection to the named table. The caller must call close on the table when finished.
// Args: conn tablename
func table(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the connection and the table name
	conn, err := ConnectionArg(c, 0)
	if err != nil {
		return nil, err
	}
	name, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	// Open the connection to the table
	tab, err := conn.ConnectToTable(ctx, string(name))
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	return c.PushingNext(rt.NewUserData(tab, getRegistryData(t).tableMt)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ConnectionArg turns a continuation argument into a *Connection.
func ConnectionArg(c *rt.GoCont, n int) (*Connection, error) {
	conn, ok := ValueToConnection(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a keyvalue connection", n+1)
	}
	return conn, nil
}

// ValueToConnection turns a lua value to a *Connection, if possible.
func ValueToConnection(v rt.Value) (*Connection, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	conn, ok := u.Value().(*Connection)
	return conn, ok
}
