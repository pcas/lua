// Postgres contains Lua helpers for the keyvalue/postgres datasource.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package postgres

import (
	"bitbucket.org/pcas/golua/lib/packagelib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue/postgres"
	"bitbucket.org/pcas/lua/lib/keyvaluelib"
	"bitbucket.org/pcas/lua/luautil"
	"errors"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// sliceToArray converts the given slice to a table.
func sliceToArray(S []string) *rt.Table {
	t := rt.NewTable()
	for i, s := range S {
		t.Set(rt.Int(i+1), rt.String(s))
	}
	return t
}

// arrayToSlice converts the array-part of the table to a slice.
func arrayToSlice(t *rt.Table) ([]string, error) {
	S := make([]string, 0)
	for i := 1; ; i++ {
		v := t.Get(rt.Int(i))
		if rt.IsNil(v) {
			return S, nil
		}
		s, ok := v.(rt.String)
		if !ok {
			return nil, fmt.Errorf("value of 'hosts[%d]' must be a string", i)
		}
		S = append(S, string(s))
	}
}

// configToTable returns a table representing the given config.
func configToTable(cfg *postgres.ClientConfig) *rt.Table {
	t := rt.NewTable()
	luautil.SetString(t, "appname", cfg.AppName)
	rt.SetEnv(t, "hosts", sliceToArray(cfg.Hosts))
	luautil.SetString(t, "username", cfg.Username)
	if cfg.PasswordSet {
		luautil.SetString(t, "password", cfg.Password)
	} else {
		luautil.SetBool(t, "password", false)
	}
	luautil.SetInt(t, "connecttimeout", cfg.ConnectTimeout)
	luautil.SetBool(t, "requiressl", cfg.RequireSSL)
	luautil.SetBool(t, "fastcopy", cfg.FastCopy)
	return t
}

// getPassword sets the password on the given config based on t.
func getPassword(cfg *postgres.ClientConfig, t *rt.Table) error {
	v := t.Get(rt.String("password"))
	if rt.IsNil(v) {
		return nil
	}
	switch x := v.(type) {
	case rt.Bool:
		if !x {
			cfg.Password = ""
			cfg.PasswordSet = false
			return nil
		}
	case rt.String:
		cfg.Password = string(x)
		cfg.PasswordSet = true
		return nil
	}
	return errors.New("value of 'password' must either be false or a string")
}

// tableToConfig returns a copy of the given config modified with the values in t.
func tableToConfig(cfg *postgres.ClientConfig, t *rt.Table) (*postgres.ClientConfig, error) {
	// Move to a copy
	if cfg == nil {
		cfg = postgres.DefaultConfig()
	} else {
		cfg = cfg.Copy()
	}
	// Modify the values
	if s, ok, err := luautil.GetString(t, "appname"); ok {
		if err != nil {
			return nil, err
		}
		cfg.AppName = s
	}
	if h, ok, err := luautil.GetTable(t, "hosts"); ok {
		if err != nil {
			return nil, err
		}
		S, err := arrayToSlice(h)
		if err != nil {
			return nil, err
		}
		cfg.Hosts = S
	}
	if s, ok, err := luautil.GetString(t, "username"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Username = s
	}
	if err := getPassword(cfg, t); err != nil {
		return nil, err
	}
	if n, ok, err := luautil.GetInt(t, "connecttimeout"); ok {
		if err != nil {
			return nil, err
		}
		cfg.ConnectTimeout = n
	}
	if b, ok, err := luautil.GetBool(t, "requiressl"); ok {
		if err != nil {
			return nil, err
		}
		cfg.RequireSSL = b
	}
	if b, ok, err := luautil.GetBool(t, "fastcopy"); ok {
		if err != nil {
			return nil, err
		}
		cfg.FastCopy = b
	}
	// Return the config settings
	return cfg, nil
}

// defaultConfig returns the default config as specified by keyvalue.<DriverName>.defaults.
func defaultConfig(t *rt.Thread) (*postgres.ClientConfig, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("keyvalue"))
	if err != nil {
		return nil, err
	}
	// Recover the defaults table
	defaults, err := luautil.Subtable(pkg, postgres.DriverName, "defaults")
	if err != nil {
		return nil, err
	}
	// Convert the values
	return tableToConfig(nil, defaults)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers us with the keyvalue package.
func init() {
	// Register our package table with the keyvalue package
	keyvaluelib.Register(postgres.DriverName, func() *rt.Table {
		// Create the package table
		pkg := rt.NewTable()
		rt.SetEnv(pkg, "defaults", configToTable(postgres.DefaultConfig()))
		rt.SetEnvGoFunc(pkg, "connect", keyvaluelib.WrapConnectFunc(connect), 2, false)
		// Return the table
		return pkg
	})
}

// connect returns a new connection to the postgres server. The caller must call close on the connection when finished. If config is nil then the default configuration settings in keyvalue.postgres.defaults will be used. The optional table config can be used to modify these defaults; in particular, config can be used to specify values for:
//	'hosts'			an array of strings specifying the hosts to connect to;
//	'username'		a string specifying the username to use when connecting;
//	'password'		either false if no password is set, or a string specifying
//					the password to use when connecting;
//	'connecttimeout'	a non-negative int specifying the maximum wait for
//					connection, in seconds;
//	'requiressl'	a bool indicating whether to require SSL;
//	'fastcopy'		a bool indicating whether to enable postgres' fast COPY;
//	'appname'		a string specifying the app name (used in logging).
//
// Args: dbname [config]
func connect(t *rt.Thread, c *rt.GoCont) (string, error) {
	// The user has to provide the database name
	if err := c.Check1Arg(); err != nil {
		return "", err
	}
	dbName, err := c.StringArg(0)
	if err != nil {
		return "", err
	}
	// Read the config
	cfg, err := defaultConfig(t)
	if err != nil {
		return "", err
	}
	if c.NArgs() > 1 {
		v, err := c.TableArg(1)
		if err != nil {
			return "", err
		}
		if cfg, err = tableToConfig(cfg, v); err != nil {
			return "", err
		}
	}
	// Validate the settings
	if err = cfg.Validate(); err != nil {
		return "", err
	}
	// Return the data source
	return cfg.URL(string(dbName)), nil
}
