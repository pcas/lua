// Reader contains Lua glue for the keyvalue reader.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvaluelib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/lua/luautil"
	"io"
	"os"
)

// closerIterator embeds a record.Iterator and closes a given io.Closer on Close.
type closerIterator struct {
	record.Iterator
	c         io.Closer // The io.Closer to closed on Close
	hasClosed bool      // Have we closed?
	err       error     // Any error on close
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// tableToOptions converts the given table to keyvalue.ReaderOptions. The keys are mapped as follows:
//	template		-> keyvalue.ReaderOptions.Template
//	allow_duplicate	-> keyvalue.ReaderOptions.AllowDuplicateKeys
//	allow_empty		-> keyvalue.ReaderOptions.AllowEmptyKeys
//	allow_missing	-> keyvalue.ReaderOptions.AllowMissingKeys
//	allow_unknown	-> keyvalue.ReaderOptions.AllowUnknownKeys
//	ignore_unknown	-> keyvalue.ReaderOptions.IgnoreUnknownKeys
func tableToOptions(t *rt.Table) (keyvalue.ReaderOptions, error) {
	opts := keyvalue.ReaderOptions{}
	// Template
	if tt, ok, err := luautil.GetTable(t, "template"); err != nil {
		return keyvalue.ReaderOptions{}, err
	} else if ok {
		if opts.Template, err = TableToRecord(tt); err != nil {
			return keyvalue.ReaderOptions{}, err
		}
	}
	// AllowDuplicateKeys
	if b, ok, err := luautil.GetBool(t, "allow_duplicate"); err != nil {
		return keyvalue.ReaderOptions{}, err
	} else if ok {
		opts.AllowDuplicateKeys = b
	}
	// AllowEmptyKeys
	if b, ok, err := luautil.GetBool(t, "allow_empty"); err != nil {
		return keyvalue.ReaderOptions{}, err
	} else if ok {
		opts.AllowEmptyKeys = b
	}
	// AllowMissingKeys
	if b, ok, err := luautil.GetBool(t, "allow_missing"); err != nil {
		return keyvalue.ReaderOptions{}, err
	} else if ok {
		opts.AllowMissingKeys = b
	}
	// AllowUnknownKeys
	if b, ok, err := luautil.GetBool(t, "allow_unknown"); err != nil {
		return keyvalue.ReaderOptions{}, err
	} else if ok {
		opts.AllowUnknownKeys = b
	}
	// IgnoreUnknownKeys
	if b, ok, err := luautil.GetBool(t, "ignore_unknown"); err != nil {
		return keyvalue.ReaderOptions{}, err
	} else if ok {
		opts.IgnoreUnknownKeys = b
	}
	return opts, nil
}

/////////////////////////////////////////////////////////////////////////
// closerIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator, preventing further iteration.
func (itr *closerIterator) Close() error {
	if !itr.hasClosed {
		itr.hasClosed = true
		// Close the iterator
		itr.err = itr.Iterator.Close()
		// Close the io.Closer
		if closeErr := itr.c.Close(); itr.err == nil {
			itr.err = closeErr
		}
	}
	return itr.err
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// open opens the named file and returns a cursor iterating over the file.
// Args: name [opts]
func open(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the file name
	var name rt.String
	if err := c.Check1Arg(); err != nil {
		return nil, err
	} else if name, err = c.StringArg(0); err != nil {
		return nil, err
	}
	// Recover the options
	opts := keyvalue.ReaderOptions{}
	if c.NArgs() > 1 && !rt.IsNil(c.Arg(1)) {
		t, err := c.TableArg(1)
		if err != nil {
			return nil, err
		}
		opts, err = tableToOptions(t)
		if err != nil {
			return nil, err
		}
	}
	// Open the file for reading
	f, err := os.Open(string(name))
	if err != nil {
		return nil, err
	}
	// Create the keyvalue reader
	itr, err := keyvalue.NewReader(f, opts)
	if err != nil {
		f.Close()
		return nil, err
	}
	// Create the cursor value
	reg := getRegistryData(t)
	cur := NewCursor(&closerIterator{
		Iterator: itr,
		c:        f,
	})
	cur.SetTimeout(reg.GetTimeout())
	return c.PushingNext(rt.NewUserData(cur, reg.cursorMt)), nil
}

// reader returns a cursor reading from the given io.Reader.
// Args: r [opts]
func reader(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the reader
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	r, err := iobindings.ReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Recover the options
	opts := keyvalue.ReaderOptions{}
	if c.NArgs() > 1 {
		t, err := c.TableArg(1)
		if err != nil {
			return nil, err
		}
		opts, err = tableToOptions(t)
		if err != nil {
			return nil, err
		}
	}
	// Create the keyvalue reader
	itr, err := keyvalue.NewReader(r, opts)
	if err != nil {
		return nil, err
	}
	// Create the cursor value
	reg := getRegistryData(t)
	cur := NewCursor(itr)
	cur.SetTimeout(reg.GetTimeout())
	return c.PushingNext(rt.NewUserData(cur, reg.cursorMt)), nil
}
