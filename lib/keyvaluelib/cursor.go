// Cursor describes a Lua view of a record.Iterator.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvaluelib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue/record"
	"context"
	"fmt"
	"io"
	"runtime"
	"time"
)

// iteratorBufferSize is the size of the iterator buffer.
const iteratorBufferSize = 1024

// Cursor describes a stream of records as given by a record.Iterator.
type Cursor struct {
	itr      record.BufferedIterator // The underlying iterator
	timeout  time.Duration           // The timeout
	isClosed bool                    // Are we closed?
	err      error                   // The error on close (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// cursorMethod wraps the GoFunction f with a check that arg(0) is a *Cursor.
func cursorMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := CursorArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// nextRecord reads the next record from the cursor.
func nextRecord(ctx context.Context, c *rt.GoCont, cur *Cursor) (rt.Cont, error) {
	// Advance the cursor
	r, err := cur.Advance(ctx)
	// Handle any errors
	if err != nil {
		if err == io.EOF {
			err = nil
		}
		return iobindings.PushingError(c, err)
	}
	// Return the record
	return c.PushingNext(RecordToTable(r)), nil
}

/////////////////////////////////////////////////////////////////////////
// Cursor functions
/////////////////////////////////////////////////////////////////////////

// NewCursor wraps the given record.Iterator as a cursor. A finaliser is set of the cursor to ensure that it is closed.
func NewCursor(itr record.Iterator) *Cursor {
	cur := &Cursor{
		itr:     record.NewBufferedIterator(itr, iteratorBufferSize),
		timeout: iobindings.NilTimeout,
	}
	runtime.SetFinalizer(cur, func(cur *Cursor) { cur.Close() })
	return cur
}

// Close closes the cursor.
func (cur *Cursor) Close() error {
	// Sanity check
	if cur == nil {
		return nil
	}
	// Is there anything to do?
	if !cur.isClosed {
		cur.isClosed = true
		err := cur.itr.Close()
		if err == nil {
			err = cur.itr.Err()
		}
		cur.err = err
		runtime.SetFinalizer(cur, nil)
	}
	return cur.err
}

// IsClosed returns true iff the cursor is closed.
func (cur *Cursor) IsClosed() bool {
	return cur == nil || cur.isClosed
}

// GetTimeout returns the timeout.
func (cur *Cursor) GetTimeout() time.Duration {
	if cur == nil {
		return 0
	}
	return cur.timeout
}

// SetTimeout sets the timeout. A negative timeout is interpreted to mean no timeout.
func (cur *Cursor) SetTimeout(timeout time.Duration) {
	if cur != nil {
		cur.timeout = timeout
	}
}

// Buffered returns true iff the next call to Advance is guaranteed not to block.
func (cur *Cursor) Buffered() bool {
	return cur.IsClosed() || cur.itr.Buffered()
}

// Advance advances the cursor. If the end of the cursor is reached, this will return io.EOF.
func (cur *Cursor) Advance(ctx context.Context) (r record.Record, err error) {
	// Is there anything to do?
	if cur.IsClosed() {
		err = io.EOF
		return
	}
	// Defer closing the cursor
	defer func() {
		if err != nil {
			if err == io.EOF {
				if closeErr := cur.Close(); closeErr != nil {
					err = closeErr
				}
			} else if err != context.DeadlineExceeded && err != context.Canceled {
				cur.Close()
			}
		}
	}()
	// Is the next record already in the buffer?
	var ok bool
	if cur.itr.Buffered() {
		ok = cur.itr.Next()
	} else {
		// If we have a non-negative timeout, create a context with timeout
		if timeout := cur.GetTimeout(); timeout >= 0 {
			var cancel context.CancelFunc
			ctx, cancel = context.WithTimeout(ctx, timeout)
			defer cancel()
		}
		// Fetch the next record
		ok, err = cur.itr.NextContext(ctx)
		if err != nil {
			return
		}
	}
	// Handle the end of the cursor
	if !ok {
		err = io.EOF
		return
	}
	// Read in the next record
	r = record.Record{}
	err = cur.itr.Scan(r)
	return
}

// String returns a string representation of a cursor.
func (cur *Cursor) String() string {
	if cur == nil {
		return "cursor(nil)"
	}
	return "cursor"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createCursorMetatable creates the metatable for a cursor.
func createCursorMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "close", cursorMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", cursorMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "settimeout", cursorMethod(iobindings.SetTimeout), 2, false)
	rt.SetEnvGoFunc(methods, "gettimeout", cursorMethod(iobindings.GetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "buffered", buffered, 1, false)
	rt.SetEnvGoFuncContext(methods, "read", read, 1, false)
	rt.SetEnvGoFunc(methods, "records", records, 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", cursorMethod(iobindings.ToString), 1, false)
	return meta
}

// buffered returns true iff the next call to read is guaranteed not to block.
// Args: cursor
func buffered(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	cur, err := CursorArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Bool(cur.Buffered())), nil
}

// read reads the next record from the cursor.
// Args: cursor
func read(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	cur, err := CursorArg(c, 0)
	if err != nil {
		return nil, err
	}
	return nextRecord(ctx, c, cur)
}

// records returns a function providing iteration over all records in the cursor.
// Args: cursor
func records(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the cursor
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	cur, err := CursorArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Create the iterator
	iterator := func(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		return nextRecord(ctx, c, cur)
	}
	// Return the iterator
	return c.PushingNext(
		rt.NewGoFunctionContext(iterator, "recorditerator", 0, false),
	), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// CursorArg turns a continuation argument into a *Cursor.
func CursorArg(c *rt.GoCont, n int) (*Cursor, error) {
	cur, ok := ValueToCursor(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a keyvalue cursor", n+1)
	}
	return cur, nil
}

// ValueToCursor turns a lua value to a *Cursor, if possible.
func ValueToCursor(v rt.Value) (*Cursor, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	cur, ok := u.Value().(*Cursor)
	return cur, ok
}
