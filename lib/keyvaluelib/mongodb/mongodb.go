// Mongodb contains Lua helpers for the keyvalue/mongodb datasource.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongodb

import (
	"bitbucket.org/pcas/golua/lib/packagelib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcas/lua/lib/keyvaluelib"
	"bitbucket.org/pcas/lua/luautil"
	"errors"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// sliceToArray converts the given slice to a table.
func sliceToArray(S []string) *rt.Table {
	t := rt.NewTable()
	for i, s := range S {
		t.Set(rt.Int(i+1), rt.String(s))
	}
	return t
}

// arrayToSlice converts the array-part of the table to a slice.
func arrayToSlice(t *rt.Table) ([]string, error) {
	S := make([]string, 0)
	for i := 1; ; i++ {
		v := t.Get(rt.Int(i))
		if rt.IsNil(v) {
			return S, nil
		}
		s, ok := v.(rt.String)
		if !ok {
			return nil, fmt.Errorf("value of 'hosts[%d]' must be a string", i)
		}
		S = append(S, string(s))
	}
}

// configToTable returns a table representing the given config.
func configToTable(cfg *mongodb.ClientConfig) *rt.Table {
	t := rt.NewTable()
	luautil.SetString(t, "appname", cfg.AppName)
	rt.SetEnv(t, "hosts", sliceToArray(cfg.Hosts))
	luautil.SetString(t, "username", cfg.Username)
	if cfg.PasswordSet {
		luautil.SetString(t, "password", cfg.Password)
	} else {
		luautil.SetBool(t, "password", false)
	}
	luautil.SetString(t, "readconcern", cfg.ReadConcern)
	if len(cfg.WriteConcern) != 0 {
		luautil.SetString(t, "writeconcern", cfg.WriteConcern)
	} else {
		luautil.SetInt(t, "writeconcern", cfg.WriteN)
	}
	luautil.SetBool(t, "writejournal", cfg.WriteJournal)
	return t
}

// getPassword sets the password on the given config based on t.
func getPassword(cfg *mongodb.ClientConfig, t *rt.Table) error {
	v := t.Get(rt.String("password"))
	if rt.IsNil(v) {
		return nil
	}
	switch x := v.(type) {
	case rt.Bool:
		if !x {
			cfg.Password = ""
			cfg.PasswordSet = false
			return nil
		}
	case rt.String:
		cfg.Password = string(x)
		cfg.PasswordSet = true
		return nil
	}
	return errors.New("value of 'password' must either be false or a string")
}

// getWriteConcern sets the write concern on the given config based on t.
func getWriteConcern(cfg *mongodb.ClientConfig, t *rt.Table) error {
	v := t.Get(rt.String("writeconcern"))
	if rt.IsNil(v) {
		return nil
	}
	switch x := v.(type) {
	case rt.Int:
		cfg.WriteConcern = ""
		cfg.WriteN = int(x)
		return nil
	case rt.String:
		cfg.WriteConcern = string(x)
		cfg.WriteN = 0
		return nil
	}
	return errors.New("value of 'writeconcern' must either be an int or a string")
}

// tableToConfig returns a copy of the given config modified with the values in t.
func tableToConfig(cfg *mongodb.ClientConfig, t *rt.Table) (*mongodb.ClientConfig, error) {
	// Move to a copy
	if cfg == nil {
		cfg = mongodb.DefaultConfig()
	} else {
		cfg = cfg.Copy()
	}
	// Modify the values
	if s, ok, err := luautil.GetString(t, "appname"); ok {
		if err != nil {
			return nil, err
		}
		cfg.AppName = s
	}
	if h, ok, err := luautil.GetTable(t, "hosts"); ok {
		if err != nil {
			return nil, err
		}
		S, err := arrayToSlice(h)
		if err != nil {
			return nil, err
		}
		cfg.Hosts = S
	}
	if s, ok, err := luautil.GetString(t, "username"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Username = s
	}
	if err := getPassword(cfg, t); err != nil {
		return nil, err
	}
	if s, ok, err := luautil.GetString(t, "readconcern"); ok {
		if err != nil {
			return nil, err
		}
		cfg.ReadConcern = s
	}
	if err := getWriteConcern(cfg, t); err != nil {
		return nil, err
	}
	if b, ok, err := luautil.GetBool(t, "writejournal"); ok {
		if err != nil {
			return nil, err
		}
		cfg.WriteJournal = b
	}
	// Return the config settings
	return cfg, nil
}

// defaultConfig returns the default config as specified by keyvalue.<DriverName>.defaults.
func defaultConfig(t *rt.Thread) (*mongodb.ClientConfig, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("keyvalue"))
	if err != nil {
		return nil, err
	}
	// Recover the defaults table
	defaults, err := luautil.Subtable(pkg, mongodb.DriverName, "defaults")
	if err != nil {
		return nil, err
	}
	// Convert the values
	return tableToConfig(nil, defaults)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers us with the keyvalue package.
func init() {
	// Register our package table with the keyvalue package
	keyvaluelib.Register(mongodb.DriverName, func() *rt.Table {
		// Create the package table
		pkg := rt.NewTable()
		rt.SetEnv(pkg, "defaults", configToTable(mongodb.DefaultConfig()))
		rt.SetEnvGoFunc(pkg, "connect", keyvaluelib.WrapConnectFunc(connect), 2, false)
		// Return the table
		return pkg
	})
}

// connect returns a new connection to the mongodb server. The caller must call close on the connection when finished. If config is nil then the default configuration settings in keyvalue.mongodb.defaults will be used. The optional table config can be used to modify these defaults; in particular, config can be used to specify values for:
//	'hosts'			an array of strings specifying the hosts to connect to;
//	'username'		a string specifying the username to use when connecting;
//	'password'		either false if no password is set, or a string specifying
//					the password to use when connecting;
//	'readconcern'	a string specifying the read concern to use (see below);
//	'writeconcern'	an int or a string specifying the write concern to use
//					(see below);
//	'writejournal'	a bool indicating whether to request acknowledgement from
//					MongoDB that write operations are written to the journal;
//	'appname'		a string specifying the app name (used in logging).
//
// If specified, the value of 'readconcern' must be one of:
//	'available'		Available specifies that the query should return data from
//					the instance with no guarantee that the data has been
//					written to a majority of the replica set members (i.e. may
//					be rolled back).
//	'linearizable'	Linearizable specifies that the query should return data
//					that reflects all successful writes issued with a write
//					concern of 'majority' and acknowledged prior to the start
//					of the read operation.
//	'local'			Local specifies that the query should return the instance’s
//					most recent data.
//	'majority'		Majority specifies that the query should return the
//					instance’s most recent data acknowledged as having been
//					written to a majority of members in the replica set.
//
// If specified, the value of 'writeconcern' must be one of:
//	'majority'		Majority requests acknowledgement that write operations
//					propagate to the majority of mongod instances.
//	N				N a positive integer. Requests acknowledgement that write
//					operations propagate to N mongod instances.
//
// Args: dbname [config]
func connect(t *rt.Thread, c *rt.GoCont) (string, error) {
	// The user has to provide the database name
	if err := c.Check1Arg(); err != nil {
		return "", err
	}
	dbName, err := c.StringArg(0)
	if err != nil {
		return "", err
	}
	// Read the config
	cfg, err := defaultConfig(t)
	if err != nil {
		return "", err
	}
	if c.NArgs() > 1 {
		v, err := c.TableArg(1)
		if err != nil {
			return "", err
		}
		if cfg, err = tableToConfig(cfg, v); err != nil {
			return "", err
		}
	}
	// Validate the settings
	if err := cfg.Validate(); err != nil {
		return "", err
	}
	// Return the data source
	return cfg.URL(string(dbName)), nil
}
