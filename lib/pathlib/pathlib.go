// Pathlib provides functions for manipulating file paths.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package pathlib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/luautil"
	"fmt"
	"path/filepath"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// wrap1Arg converts the given function to a GoFunction.
func wrap1Arg(f func(string) rt.Value) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		s, err := c.StringArg(0)
		if err != nil {
			return nil, err
		}
		return c.PushingNext(f(string(s))), nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "path",
		Description: "Functions for manipulating file paths.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			pkg := rt.NewTable()
			luautil.SetString(pkg, "sep", string(filepath.Separator))
			luautil.SetString(pkg, "delimiter", string(filepath.ListSeparator))
			rt.SetEnvGoFunc(pkg, "normalize", wrap1Arg(normalize), 1, false)
			rt.SetEnvGoFunc(pkg, "join", join, 0, true)
			rt.SetEnvGoFunc(pkg, "resolve", resolve, 1, false)
			rt.SetEnvGoFunc(pkg, "dirname", wrap1Arg(dirname), 1, false)
			rt.SetEnvGoFunc(pkg, "basename", wrap1Arg(basename), 1, false)
			rt.SetEnvGoFunc(pkg, "extname", wrap1Arg(extname), 1, false)
			rt.SetEnvGoFunc(pkg, "isabsolute", wrap1Arg(isabsolute), 1, false)
			rt.SetEnvGoFunc(pkg, "fromslash", wrap1Arg(fromslash), 1, false)
			rt.SetEnvGoFunc(pkg, "toslash", wrap1Arg(toslash), 1, false)
			rt.SetEnvGoFunc(pkg, "split", split, 1, false)
			rt.SetEnvGoFunc(pkg, "relative", relative, 2, false)
			rt.SetEnvGoFunc(pkg, "realpath", realpath, 1, false)
			return pkg, nil
		},
	})
}

// normalize returns the shortest path name equivalent to the given path by purely lexical processing. It applies the following rules iteratively until no further processing can be done:
//
//	1. Replace multiple "/" elements with a single one.
//	2. Eliminate each . path name element (the current directory).
//	3. Eliminate each inner .. path name element (the parent directory)
//	   along with the non-.. element that precedes it.
//	4. Eliminate .. elements that begin a rooted path:
//	   that is, replace "/.." by "/" at the beginning of a path.
// The returned path ends in a slash only if it represents a root directory.
//
// If the result of this process is an empty string, normalize returns ".".
//
// See also Rob Pike, "Lexical File Names in Plan 9 or Getting Dot-Dot Right"
//		https://9p.io/sys/doc/lexnames.html
// Args: path
func normalize(s string) rt.Value {
	return rt.String(filepath.Clean(s))
}

// join joins any number of path elements into a single path, adding a separator if necessary. The returned path is normalized; in particular, all empty strings are ignored.
// Args: [elem1 [... elemn]]
func join(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	etc := c.Etc()
	elems := make([]string, 0, len(etc))
	for i, v := range etc {
		elem, ok := rt.AsString(v)
		if !ok {
			return nil, fmt.Errorf("#%d must be a string", i+1)
		}
		elems = append(elems, string(elem))
	}
	return c.PushingNext(rt.String(filepath.Join(elems...))), nil
}

// resolve returns an absolute representation of the given path. If the path is not absolute it will be joined with the current working directory to turn it into an absolute path. The absolute path name for a given file is not guaranteed to be unique. The returned path is normalized.
// Args: path
func resolve(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	path, err := filepath.Abs(string(s))
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(path)), nil
}

// dirname returns all but the last element of the given path, typically the path's directory. The returned path will be normalized. If the path is empty, dirname returns ".". If the path consists entirely of separators, dirname returns a single separator. The returned path does not end in a separator unless it is the root directory.
// Args: path
func dirname(s string) rt.Value {
	return rt.String(filepath.Dir(s))
}

// basename returns the last element of the given path. Trailing path separators are removed before extracting the last element. If the path is empty, basename returns ".". If the path consists entirely of separators, basename returns a single separator.
// Args: path
func basename(s string) rt.Value {
	return rt.String(filepath.Base(s))
}

// extname returns the file name extension used by the given path. The extension is the suffix beginning at the final dot in the final element of path; it is empty if there is no dot.
// Args: path
func extname(s string) rt.Value {
	return rt.String(filepath.Ext(s))
}

// isabsolute returns true iff the given path is absolute.
// Args: path
func isabsolute(s string) rt.Value {
	return rt.Bool(filepath.IsAbs(s))
}

// fromslash returns the result of replacing each slash ('/') character in path with a separator character. Multiple slashes are replaced by multiple separators.
// Args: path
func fromslash(s string) rt.Value {
	return rt.String(filepath.FromSlash(s))
}

// toslash returns the result of replacing each separator character in path with a slash ('/') character. Multiple separators are replaced by multiple slashes.
// Args: path
func toslash(s string) rt.Value {
	return rt.String(filepath.ToSlash(s))
}

// split splits path immediately following the final Separator, separating it into a directory and file name component. If there is no Separator in path, Split returns an empty dir and file set to path. The returned values have the property that path = dir .. file.
func split(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	dir, file := filepath.Split(string(s))
	next := c.Next()
	next.Push(rt.String(dir))
	next.Push(rt.String(file))
	return next, nil
}

// relative returns a relative path that is lexically equivalent to #2 (the target path) when joined to #1 (the base path) with an intervening separator. That is,
//		path.join(base, path.relative(base, target))
// is equivalent to target itself.
//
// The returned path will always be relative to the base path, even if the base path and the target path share no elements. This will throw an error if the target path can not be made relative to the base path, or if knowing the current working directory would be necessary to compute it. The returned path is normalized.
// Args: basepath targetpath
func relative(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	base, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	target, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	path, err := filepath.Rel(string(base), string(target))
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(path)), nil
}

// realpath returns the path name after the evaluation of any symbolic links. If path is relative the result will be relative to the current directory, unless one of the components is an absolute symbolic link.
// Args: path
func realpath(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	p, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	path, err := filepath.EvalSymlinks(string(p))
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(path)), nil
}
