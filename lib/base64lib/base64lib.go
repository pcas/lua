// Base64 provides base64 encoding and decoding.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package base64lib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"encoding/base64"
	"fmt"
	"sort"
	"strings"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	encoderMt *rt.Table // The metatable for an encoder
	decoderMt *rt.Table // The metatable for a decoder
}

// encodingMap is a map from names to encodings.
var encodingMap = map[string]*base64.Encoding{
	"rawstd": base64.RawStdEncoding,
	"rawurl": base64.RawStdEncoding,
	"std":    base64.StdEncoding,
	"url":    base64.URLEncoding,
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// createEtypes returns a table listing the valid etypes.
func createEtypes() *rt.Table {
	S := make([]string, 0, len(encodingMap))
	for k := range encodingMap {
		S = append(S, k)
	}
	sort.Strings(S)
	t := rt.NewTable()
	for i, k := range S {
		t.Set(rt.Int(i+1), rt.String(k))
	}
	return t
}

// recoverEncoding returns the base64.Encoding with name given by the n-th continuation argument.
func recoverEncoding(c *rt.GoCont, n int) (*base64.Encoding, error) {
	s, err := c.StringArg(n)
	if err != nil {
		return nil, err
	}
	enc, ok := encodingMap[strings.ToLower(string(s))]
	if !ok {
		return nil, fmt.Errorf("#%d is not a valid etype", n+1)
	}
	return enc, nil
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "base64",
		Description: "Implements base64 encoding and decoding.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				encoderMt: createEncoderMetatable(),
				decoderMt: createDecoderMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnv(pkg, "etypes", createEtypes())
			rt.SetEnvGoFunc(pkg, "encoder", encoder, 2, false)
			rt.SetEnvGoFunc(pkg, "decoder", decoder, 2, false)
			return pkg, nil
		},
	})
}

// encoder returns a new base64 encoder wrapping the given writer using the encoding specified by etype. The caller must call close on the encoder when finished to ensure that any buffered data is flushed to the writer.
// Args: etype writer
func encoder(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the encoding
	enc, err := recoverEncoding(c, 0)
	if err != nil {
		return nil, err
	}
	// Recover the writer
	w, err := iobindings.WriterArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Create the encoder
	return c.PushingNext(rt.NewUserData(NewEncoder(enc, w), getRegistryData(t).encoderMt)), nil
}

// decoder returns a new base64 decoder wrapping the given reader using the encoding specified by etype.
// Args: etype reader
func decoder(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the encoding
	enc, err := recoverEncoding(c, 0)
	if err != nil {
		return nil, err
	}
	// Recover the reader
	r, err := iobindings.ReaderArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Create the decoder
	return c.PushingNext(rt.NewUserData(NewDecoder(enc, r), getRegistryData(t).decoderMt)), nil
}
