// Jsonlib provides a lightweight JSON library for Lua.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package jsonlib

import (
	"bitbucket.org/pcas/golua/lib/oslib"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "json",
		Description: "A lightweight JSON library.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "encode", encode, 2, false)
			rt.SetEnvGoFunc(pkg, "decode", decode, 1, false)
			return pkg, nil
		},
	})
}

// encode returns a string representing the value encoded in JSON. If the second argument is true then the JSON will be pretty-printed.
// Args: value [prettyprint]
func encode(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	var b []byte
	var err error
	if c.NArgs() > 1 && rt.Truth(c.Arg(1)) {
		b, err = json.MarshalIndent(c.Arg(0), "", "\t")
	} else {
		b, err = json.Marshal(c.Arg(0))
	}
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(string(b))), nil
}

// decode returns a value representing the decoded JSON string.
// Args: string
func decode(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	var x interface{}
	if err = json.Unmarshal([]byte(s), &x); err != nil {
		return nil, err
	}
	return c.PushingNext(ToValue(x)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToValue attempts to convert x to a Lua value. Supported types are
//	string
//	bool
//	int, int8, int16, int32, int64
//	uint, uint8, uint16, uint32, uint64
//	float32, float64
//	time.Time
//	[]string
//	[]bool
//	[]int, []int8, []int16, []int32, []int64
//	[]uint, []uint8, []uint16, []uint32, []uint64
//	[]float32, []float64
//	[]time.Time
//	[]interface{}
//	map[string]interface{}
//	fmt.Stringer
// and recursion on these types where appropriate. Note that no attempt is made to detect cyclic data-structures.
func ToValue(x interface{}) rt.Value {
	if x == nil {
		return rt.Value(nil)
	}
	switch t := x.(type) {
	case string:
		return rt.String(t)
	case bool:
		return rt.Bool(t)
	case int:
		return rt.Int(t)
	case int8:
		return rt.Int(t)
	case int16:
		return rt.Int(t)
	case int32:
		return rt.Int(t)
	case int64:
		return rt.Int(t)
	case uint:
		if t <= math.MaxInt64 {
			return rt.Int(t)
		}
		return rt.String(strconv.FormatUint(uint64(t), 10))
	case uint8:
		return rt.Int(t)
	case uint16:
		return rt.Int(t)
	case uint32:
		return rt.Int(t)
	case uint64:
		if t <= math.MaxInt64 {
			return rt.Int(t)
		}
		return rt.String(strconv.FormatUint(t, 10))
	case float32:
		return rt.Float(t)
	case float64:
		return rt.Float(t)
	case time.Time:
		return oslib.TimeToTable(t)
	case []string:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.String(x))
		}
		return v
	case []bool:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Bool(x))
		}
		return v
	case []int:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []int8:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []int16:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []int32:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []int64:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []uint:
		v := rt.NewTable()
		for i, x := range t {
			if x <= math.MaxInt64 {
				v.Set(rt.Int(i+1), rt.Int(x))
			} else {
				v.Set(rt.Int(i+1), rt.String(strconv.FormatUint(uint64(x), 10)))
			}
		}
		return v
	case []uint8:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []uint16:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []uint32:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Int(x))
		}
		return v
	case []uint64:
		v := rt.NewTable()
		for i, x := range t {
			if x <= math.MaxInt64 {
				v.Set(rt.Int(i+1), rt.Int(x))
			} else {
				v.Set(rt.Int(i+1), rt.String(strconv.FormatUint(x, 10)))
			}
		}
		return v
	case []float32:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Float(x))
		}
		return v
	case []float64:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), rt.Float(x))
		}
		return v
	case []time.Time:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), oslib.TimeToTable(x))
		}
		return v
	case []interface{}:
		v := rt.NewTable()
		for i, x := range t {
			v.Set(rt.Int(i+1), ToValue(x))
		}
		return v
	case map[string]interface{}:
		v := rt.NewTable()
		for k, x := range t {
			rt.SetEnv(v, k, ToValue(x))
		}
		return v
	case fmt.Stringer:
		return rt.String(t.String())
	default:
		return rt.String("<unknown data>")
	}
}
