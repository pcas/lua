// Stringutillib implements simple functions to manipulate Lua strings.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package stringutillib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"strings"
)

// maxInt defines the maximum size of an int.
const maxInt = int64(int(^uint(0) >> 1))

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// wrap2Args converts the given function to a GoFunction.
func wrap2Args(f func(string, string) rt.Value) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.CheckNArgs(2); err != nil {
			return nil, err
		}
		s1, err := c.StringArg(0)
		if err != nil {
			return nil, err
		}
		s2, err := c.StringArg(1)
		if err != nil {
			return nil, err
		}
		return c.PushingNext(f(string(s1), string(s2))), nil
	}
}

// sliceToTable returns a *rt.Table representing S.
func sliceToTable(S []string) *rt.Table {
	t := rt.NewTable()
	for i, s := range S {
		t.Set(rt.Int(i+1), rt.String(s))
	}
	return t
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "stringutil",
		Description: "Implements simple functions to manipulate strings.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "contains", wrap2Args(contains), 2, false)
			rt.SetEnvGoFunc(pkg, "containsany", wrap2Args(containsany), 2, false)
			rt.SetEnvGoFunc(pkg, "count", wrap2Args(count), 2, false)
			rt.SetEnvGoFunc(pkg, "hasprefix", wrap2Args(hasprefix), 2, false)
			rt.SetEnvGoFunc(pkg, "hassuffix", wrap2Args(hassuffix), 2, false)
			rt.SetEnvGoFunc(pkg, "index", wrap2Args(index), 2, false)
			rt.SetEnvGoFunc(pkg, "indexany", wrap2Args(indexany), 2, false)
			rt.SetEnvGoFunc(pkg, "lastindex", wrap2Args(lastindex), 2, false)
			rt.SetEnvGoFunc(pkg, "lastindexany", wrap2Args(lastindexany), 2, false)
			rt.SetEnvGoFunc(pkg, "split", wrap2Args(split), 2, false)
			rt.SetEnvGoFunc(pkg, "splitafter", wrap2Args(splitafter), 2, false)
			rt.SetEnvGoFunc(pkg, "trim", wrap2Args(trim), 2, false)
			rt.SetEnvGoFunc(pkg, "trimleft", wrap2Args(trimleft), 2, false)
			rt.SetEnvGoFunc(pkg, "trimprefix", wrap2Args(trimprefix), 2, false)
			rt.SetEnvGoFunc(pkg, "trimright", wrap2Args(trimright), 2, false)
			rt.SetEnvGoFunc(pkg, "trimsuffix", wrap2Args(trimsuffix), 2, false)
			rt.SetEnvGoFunc(pkg, "trimspace", trimspace, 1, false)
			rt.SetEnvGoFunc(pkg, "splitaftern", splitaftern, 3, false)
			rt.SetEnvGoFunc(pkg, "splitn", splitn, 3, false)
			rt.SetEnvGoFunc(pkg, "replace", replace, 4, false)
			rt.SetEnvGoFunc(pkg, "replaceall", replaceall, 3, false)
			rt.SetEnvGoFunc(pkg, "join", join, 2, false)

			return pkg, nil
		},
	})
}

// contains returns true iff substr is within s.
// Args: s substr
func contains(s string, substr string) rt.Value {
	return rt.Bool(strings.Contains(s, substr))
}

// containsany returns true iff any Unicode code points in chars are within s.
// Args: s chars
func containsany(s string, chars string) rt.Value {
	return rt.Bool(strings.ContainsAny(s, chars))
}

// count counts the number of non-overlapping instances of substr in s. If substr is an empty string, count returns 1 + the number of Unicode code points in s.
// Args: s substr
func count(s string, substr string) rt.Value {
	return rt.Int(strings.Count(s, substr))
}

// hasprefix returns true iff the string s begins with prefix.
// Args: s prefix
func hasprefix(s string, prefix string) rt.Value {
	return rt.Bool(strings.HasPrefix(s, prefix))
}

// hassuffix returns true iff the string s ends with suffix.
// Args: s suffix
func hassuffix(s string, suffix string) rt.Value {
	return rt.Bool(strings.HasSuffix(s, suffix))
}

// index returns the index of the first instance of substr in s, or nil if substr is not present in s.
// Args: s substr
func index(s string, substr string) rt.Value {
	if idx := strings.Index(s, substr); idx != -1 {
		return rt.Int(idx + 1)
	}
	return nil
}

// indexany returns the index of the first instance of any Unicode code point from chars in s, or nil if no Unicode code point from chars is present in s.
// Args: s chars
func indexany(s string, chars string) rt.Value {
	if idx := strings.IndexAny(s, chars); idx != -1 {
		return rt.Int(idx + 1)
	}
	return nil
}

// lastindex returns the index of the last instance of substr in s, or nil if substr is not present in s.
// Args: s substr
func lastindex(s string, substr string) rt.Value {
	if idx := strings.LastIndex(s, substr); idx != -1 {
		return rt.Int(idx + 1)
	}
	return nil
}

// lastindexany returns the index of the last instance of any Unicode code point from chars in s, or nil if no Unicode code point from chars is present in s.
// Args: s chars
func lastindexany(s string, chars string) rt.Value {
	if idx := strings.LastIndexAny(s, chars); idx != -1 {
		return rt.Int(idx + 1)
	}
	return nil
}

// split slices s into all substrings separated by sep and returns an array of the substrings between those separators.
//
// If s does not contain sep and sep is not empty, split returns an array of length 1 whose only element is s. If sep is empty, split splits after each UTF-8 sequence. If both s and sep are empty, split returns an empty array.
// Args: s sep
func split(s string, sep string) rt.Value {
	return sliceToTable(strings.Split(s, sep))
}

// splitafter slices s into all substrings after each instance of sep and returns an array of those substrings.
//
// If s does not contain sep and sep is not empty, splitafter returns an array of length 1 whose only element is s. If sep is empty, splitafter splits after each UTF-8 sequence. If both s and sep are empty, splitafter returns an empty array.
// Args: s sep
func splitafter(s string, sep string) rt.Value {
	return sliceToTable(strings.SplitAfter(s, sep))
}

// trim returns a substring of the string s with all leading and trailing Unicode code points contained in cutset removed.
// Args: s cutset
func trim(s string, cutset string) rt.Value {
	return rt.String(strings.Trim(s, cutset))
}

// trimleft returns a substring of the string s with all leading Unicode code points contained in cutset removed.
// Args: s cutset
func trimleft(s string, cutset string) rt.Value {
	return rt.String(strings.TrimLeft(s, cutset))
}

// trimprefix returns s without the provided leading prefix string. If s does not start with prefix, s is returned unchanged.
// Args: s prefix
func trimprefix(s string, prefix string) rt.Value {
	return rt.String(strings.TrimPrefix(s, prefix))
}

// trimright returns a substring of the string s, with all trailing Unicode code points contained in cutset removed.
// Args: s cutset
func trimright(s string, cutset string) rt.Value {
	return rt.String(strings.TrimRight(s, cutset))
}

// trimsuffix returns s without the provided trailing suffix string. If s does not end with suffix, s is returned unchanged.
// Args: s suffix
func trimsuffix(s string, suffix string) rt.Value {
	return rt.String(strings.TrimSuffix(s, suffix))
}

// trimspace returns s without the provided trailing suffix string. If s does not end with suffix, s is returned unchanged.
// Args: s
func trimspace(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(strings.TrimSpace(string(s)))), nil
}

// splitaftern slices s into substrings after each instance of sep and returns an array of those substrings.
//
// The value n determines the number of substrings to return:
//	n > 0: at most n substrings; the last substring will be the unsplit remainder;
//	n == 0: the result is an empty array (zero substrings);
//	n is nil or n < 0: all substrings.
//
// Edge cases for s and sep (for example, empty strings) are handled as described in the documentation for splitafter.
// Args: s sep [n]
func splitaftern(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	sep, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	n := -1
	if c.NArgs() > 2 {
		if k, err := c.IntArg(2); err != nil {
			return nil, err
		} else if int64(k) > maxInt {
			return nil, errors.New("#3 out-of-range")
		} else if k >= 0 {
			n = int(k)
		}
	}
	return c.PushingNext(sliceToTable(strings.SplitAfterN(string(s), string(sep), n))), nil
}

// splitn slices s into substrings separated by sep and returns an array of the substrings between those separators.
//
// The value n determines the number of substrings to return:
//	n > 0: at most n substrings; the last substring will be the unsplit remainder;
//	n == 0: the result is an empty array (zero substrings);
//	n is nil or n < 0: all substrings.
//
// Edge cases for s and sep (for example, empty strings) are handled as described in the documentation for split.
// Args: s sep [n]
func splitn(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	sep, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	n := -1
	if c.NArgs() > 2 {
		if k, err := c.IntArg(2); err != nil {
			return nil, err
		} else if int64(k) > maxInt {
			return nil, errors.New("#3 out-of-range")
		} else if k >= 0 {
			n = int(k)
		}
	}
	return c.PushingNext(sliceToTable(strings.SplitN(string(s), string(sep), n))), nil
}

// replace returns a copy of the string s with the first n non-overlapping instances of old replaced by new. If old is empty, it matches at the beginning of the string and after each UTF-8 sequence, yielding up to k+1 replacements for a k-rune string. If n is nil or n < 0, there is no limit on the number of replacements.
// Args: s old new [n]
func replace(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	olds, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	news, err := c.StringArg(2)
	if err != nil {
		return nil, err
	}
	n := -1
	if c.NArgs() > 3 {
		if k, err := c.IntArg(3); err != nil {
			return nil, err
		} else if int64(k) > maxInt {
			return nil, errors.New("#4 out-of-range")
		} else if k >= 0 {
			n = int(k)
		}
	}
	return c.PushingNext(rt.String(strings.Replace(string(s), string(olds), string(news), n))), nil
}

// replaceall returns a copy of the string s with all non-overlapping instances of old replaced by new. If old is empty, it matches at the beginning of the string and after each UTF-8 sequence, yielding up to k+1 replacements for a k-rune string.
// Args: s old new
func replaceall(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	olds, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	news, err := c.StringArg(2)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(strings.ReplaceAll(string(s), string(olds), string(news)))), nil
}

// join concatenates the elements of S to create a single string. The separator string sep is placed between elements in the resulting string.
// Args: S sep
func join(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	t, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	sep, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	S := make([]string, 0)
	i := 1
	for {
		v := t.Get(rt.Int(i))
		if rt.IsNil(v) {
			break
		}
		s, ok := rt.AsString(v)
		if !ok {
			break
		}
		S = append(S, string(s))
		i++
	}
	return c.PushingNext(rt.String(strings.Join(S, string(sep)))), nil
}
