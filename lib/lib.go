// Lib imports the standard library.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lib

import (
	// Blank imports for the library packages
	_ "bitbucket.org/pcas/golua/lib"
	_ "bitbucket.org/pcas/lua/lib/base64lib"
	_ "bitbucket.org/pcas/lua/lib/binarylib"
	_ "bitbucket.org/pcas/lua/lib/compressorlib"
	_ "bitbucket.org/pcas/lua/lib/cryptolib"
	_ "bitbucket.org/pcas/lua/lib/filelib"
	_ "bitbucket.org/pcas/lua/lib/fslib"
	_ "bitbucket.org/pcas/lua/lib/fslib/filesystem"
	_ "bitbucket.org/pcas/lua/lib/fslib/fsd"
	_ "bitbucket.org/pcas/lua/lib/fslib/seaweed"
	_ "bitbucket.org/pcas/lua/lib/ioutillib"
	_ "bitbucket.org/pcas/lua/lib/joblib"
	_ "bitbucket.org/pcas/lua/lib/jsonlib"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib/kvdb"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib/mongodb"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib/postgres"
	_ "bitbucket.org/pcas/lua/lib/logginglib"
	_ "bitbucket.org/pcas/lua/lib/monitoringlib"
	_ "bitbucket.org/pcas/lua/lib/osutillib"
	_ "bitbucket.org/pcas/lua/lib/pathlib"
	_ "bitbucket.org/pcas/lua/lib/stringutillib"
	_ "bitbucket.org/pcas/lua/lib/tableutillib"
	_ "bitbucket.org/pcas/lua/lib/ulidlib"
	_ "bitbucket.org/pcas/lua/lib/urllib"
)
