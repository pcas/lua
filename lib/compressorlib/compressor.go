// Compressor provides a Lua-view of a compressor and decompressor.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package compressorlib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/compress"
	"fmt"
	"io"
	"runtime"
)

// Compressor describes a compressor.
type Compressor struct {
	io.WriteCloser
	t        compress.Type // The compression type
	isClosed bool          // Is the compressor closed?
	closeErr error         // The error on close (if any)
}

// Decompressor describes a decompressor.
type Decompressor struct {
	io.ReadCloser
	t        compress.Type // The compression type
	isClosed bool          // Is the decompressor closed?
	closeErr error         // The error on close (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// compressorMethod wraps the GoFunction f with a check that arg(0) is a *Compressor.
func compressorMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := CompressorArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// decompressorMethod wraps the GoFunction f with a check that arg(0) is a *Decompressor.
func decompressorMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := DecompressorArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

/////////////////////////////////////////////////////////////////////////
// Compressor functions
/////////////////////////////////////////////////////////////////////////

// NewCompressor returns a new compressor of type t wrapping w. A finaliser is set on the returned compressor (via runtime.SetFinalizer) so that it is automatically closed when garbage collected.
func NewCompressor(t compress.Type, w io.Writer) (*Compressor, error) {
	// Create the underlying compressor
	cw, err := t.NewWriter(w)
	if err != nil {
		return nil, err
	}
	// Wrap it up and set the finaliser
	c := &Compressor{
		WriteCloser: cw,
		t:           t,
	}
	runtime.SetFinalizer(c, func(c *Compressor) { c.Close() })
	return c, nil
}

// Close closes the compressor.
func (c *Compressor) Close() error {
	if c == nil {
		return nil
	} else if !c.isClosed {
		c.closeErr = c.WriteCloser.Close()
		c.isClosed = true
		runtime.SetFinalizer(c, nil)
	}
	return c.closeErr
}

// IsClosed returns true iff the compressor is known to be closed.
func (c *Compressor) IsClosed() bool {
	return c == nil || c.isClosed
}

// Flush flushes any cached data.
func (c *Compressor) Flush() error {
	if !c.IsClosed() {
		if fl, ok := c.WriteCloser.(iobindings.Flusher); ok {
			return fl.Flush()
		}
	}
	return nil
}

// Type returns the compressor type.
func (c *Compressor) Type() compress.Type {
	if c == nil {
		return compress.None
	}
	return c.t
}

// String returns a string description of the compressor.
func (c *Compressor) String() string {
	return "compressor(" + c.Type().String() + ")"
}

/////////////////////////////////////////////////////////////////////////
// Decompressor functions
/////////////////////////////////////////////////////////////////////////

// NewDecompressor returns a new decompressor of type t wrapping r. A finaliser is set on the returned decompressor (via runtime.SetFinalizer) so that it is automatically closed when garbage collected.
func NewDecompressor(t compress.Type, r io.Reader) (*Decompressor, error) {
	// Create the underlying decompressor
	cr, err := t.NewReader(r)
	if err != nil {
		return nil, err
	}
	// Wrap it up and set the finaliser
	c := &Decompressor{
		ReadCloser: cr,
		t:          t,
	}
	runtime.SetFinalizer(c, func(c *Decompressor) { c.Close() })
	return c, nil
}

// Close closes the decompressor.
func (c *Decompressor) Close() error {
	if c == nil {
		return nil
	} else if !c.isClosed {
		c.closeErr = c.ReadCloser.Close()
		c.isClosed = true
		runtime.SetFinalizer(c, nil)
	}
	return c.closeErr
}

// IsClosed returns true iff the decompressor is known to be closed.
func (c *Decompressor) IsClosed() bool {
	return c == nil || c.isClosed
}

// Type returns the decompressor type.
func (c *Decompressor) Type() compress.Type {
	if c == nil {
		return compress.None
	}
	return c.t
}

// String returns a string description of the decompressor.
func (c *Decompressor) String() string {
	return "decompressor(" + c.Type().String() + ")"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createCompressorMetatable creates the metatable for a compressor.
func createCompressorMetatable() *rt.Table {
	// Create the methods for a compressor
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "close", compressorMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", compressorMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "flush", compressorMethod(iobindings.Flush), 1, false)
	rt.SetEnvGoFunc(methods, "write", compressorMethod(iobindings.Write), 1, true)
	// Create the metatable for a compressor
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", compressorMethod(iobindings.ToString), 1, false)
	return meta
}

// createDecompressorMetatable creates the metatable for a decompressor.
func createDecompressorMetatable() *rt.Table {
	// Create the methods for a decompressor
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", decompressorMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", decompressorMethod(iobindings.Lines), 1, true)
	rt.SetEnvGoFunc(methods, "close", decompressorMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", decompressorMethod(iobindings.IsClosed), 1, false)
	// Create the metatable for a decompressor
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", decompressorMethod(iobindings.ToString), 1, false)
	return meta
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// CompressorArg turns a continuation argument into a *Compressor.
func CompressorArg(c *rt.GoCont, n int) (*Compressor, error) {
	w, ok := ValueToCompressor(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a compressor", n+1)
	}
	return w, nil
}

// ValueToCompressor turns a Lua value to a *Compressor if possible.
func ValueToCompressor(v rt.Value) (*Compressor, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	w, ok := u.Value().(*Compressor)
	return w, ok
}

// DecompressorArg turns a continuation argument into a *Decompressor.
func DecompressorArg(c *rt.GoCont, n int) (*Decompressor, error) {
	r, ok := ValueToDecompressor(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a decompressor", n+1)
	}
	return r, nil
}

// ValueToDecompressor turns a Lua value to a *Decompressor if possible.
func ValueToDecompressor(v rt.Value) (*Decompressor, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	r, ok := u.Value().(*Decompressor)
	return r, ok
}
