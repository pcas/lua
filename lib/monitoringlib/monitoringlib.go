// Monitoringlib contains Lua glue for the monitord package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitoringlib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/lib/packagelib"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcas/lua/luautil"
	"bitbucket.org/pcastools/address"
	"time"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	timeout   time.Duration // The default timeout
	monitorMt *rt.Table     // The metatable for a monitor
	streamMt  *rt.Table     // The metatable for a stream
	entryMt   *rt.Table     // The metatable for an entry
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// configToTable returns a table representing the given client configuration.
func configToTable(cfg *monitord.ClientConfig) *rt.Table {
	t := rt.NewTable()
	luautil.SetString(t, "address", cfg.URI())
	luautil.SetString(t, "sslcert", string(cfg.SSLCert))
	return t
}

// tableToConfig returns a copy of the given client configuration modified with the values in t.
func tableToConfig(cfg *monitord.ClientConfig, t *rt.Table) (*monitord.ClientConfig, error) {
	// Move to a copy
	if cfg == nil {
		cfg = monitord.DefaultConfig()
	} else {
		cfg = cfg.Copy()
	}
	// Modify the values
	if s, ok, err := luautil.GetString(t, "address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	if s, ok, err := luautil.GetString(t, "sslcert"); ok {
		if err != nil {
			return nil, err
		}
		cfg.SSLCert = []byte(s)
	}
	// Return the config settings
	return cfg, nil
}

// defaultConfig returns the default client configuration as specified by monitoring.defaults.
func defaultConfig(t *rt.Thread) (*monitord.ClientConfig, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("monitoring"))
	if err != nil {
		return nil, err
	}
	// Recover the defaults table
	defaults, err := luautil.Subtable(pkg, "defaults")
	if err != nil {
		return nil, err
	}
	// Convert the values
	return tableToConfig(nil, defaults)
}

/////////////////////////////////////////////////////////////////////////
// registryData functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// SetTimeout sets the default timeout.
func (r *registryData) SetTimeout(timeout time.Duration) {
	r.timeout = timeout
}

// GetTimeout returns the default timeout.
func (r *registryData) GetTimeout() time.Duration {
	return r.timeout
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	// Register this module
	module.Register(module.Info{
		Name:        "monitoring",
		Description: "Provides monitoring of logs.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				timeout:   iobindings.NilTimeout,
				monitorMt: createMonitorMetatable(),
				streamMt:  createStreamMetatable(),
				entryMt:   createEntryMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnv(pkg, "defaults", configToTable(monitord.DefaultConfig()))
			rt.SetEnvGoFunc(pkg, "connect", connect, 1, false)
			rt.SetEnvGoFunc(pkg, "settimeout", settimeout, 1, false)
			rt.SetEnvGoFunc(pkg, "gettimeout", gettimeout, 0, false)
			return pkg, nil
		},
	})
}

// settimeout sets the default timeout.
func settimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.SetTimeoutWithSetTimeouter(getRegistryData(t), c)
}

// gettimeout returns the default I/O timeout, in seconds.
func gettimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.GetTimeoutWithGetTimeouter(getRegistryData(t), c)
}
