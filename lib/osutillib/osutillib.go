// Ssutillib provides additional os functionality.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package osutillib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/luautil"
	"context"
	"errors"
	"fmt"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/process"
	"net"
	"os"
	"os/user"
	"runtime"
	"strconv"
	"strings"
	"time"
)

// Args is a slice of command-line arguments to be exposed to the Lua runtime. Note that changing the value of Args after the package is loaded into the Lua runtime will have no effect.
var Args []string

// maxInt defines the maximum size of an int.
const maxInt = int64(int(^uint(0) >> 1))

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createArgsList returns the command-line arguments.
func createArgsList() *rt.Table {
	t := rt.NewTable()
	for i, arg := range Args {
		t.Set(rt.Int(i+1), rt.String(arg))
	}
	return t
}

// convertError converts the given error.
func convertError(c rt.Cont, err error) error {
	if err == context.DeadlineExceeded {
		return errors.New("timeout")
	} else if err == context.Canceled {
		return errors.New("user interrupt")
	}
	return err
}

// wrap0Arg converts the given function to a GoFunction.
func wrap0Arg(f func() (rt.Value, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		v, err := f()
		if err != nil {
			return nil, convertError(c, err)
		}
		return c.PushingNext(v), nil
	}
}

// wrap0ArgContext converts the given function to a GoFunction with context.
func wrap0ArgContext(f func(context.Context) (rt.Value, error)) func(context.Context, *rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		v, err := f(ctx)
		if err != nil {
			return nil, convertError(c, err)
		}
		return c.PushingNext(v), nil
	}
}

// wrap1Arg converts the given function to a GoFunction.
func wrap1Arg(f func(string) (rt.Value, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		s, err := c.StringArg(0)
		if err != nil {
			return nil, err
		}
		v, e := f(string(s))
		if e != nil {
			return nil, convertError(c, e)
		}
		return c.PushingNext(v), nil
	}
}

// valueToCPUUsage attempts to convert the indicated argument to a table with entries:
//	{ "user": float64, "system": float64 }
// Returns the values of user and system on success.
func valueToCPUUsage(c *rt.GoCont, n int) (float64, float64, error) {
	t, err := c.TableArg(n)
	if err != nil {
		return 0, 0, err
	}
	user, ok := rt.ToFloat(t.Get(rt.String("user")))
	if !ok {
		return 0, 0, fmt.Errorf("#%d must have 'user' with value a float", n+1)
	}
	system, ok := rt.ToFloat(t.Get(rt.String("system")))
	if !ok {
		return 0, 0, fmt.Errorf("#%d must have 'system' with value a float", n+1)
	}
	return float64(user), float64(system), nil
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "osutil",
		Description: "Provides information about the OS.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnv(pkg, "args", createArgsList())
			rt.SetEnvGoFunc(pkg, "arch", wrap0Arg(arch), 0, false)
			rt.SetEnvGoFunc(pkg, "chdir", wrap1Arg(chdir), 1, false)
			rt.SetEnvGoFuncContext(pkg, "cpus", wrap0ArgContext(cpus), 0, false)
			rt.SetEnvGoFuncContext(pkg, "cpuusage", cpuusage, 1, false)
			rt.SetEnvGoFunc(pkg, "cwd", wrap0Arg(cwd), 0, false)
			rt.SetEnvGoFunc(pkg, "env", wrap0Arg(env), 0, false)
			rt.SetEnvGoFunc(pkg, "setenv", setenv, 2, false)
			rt.SetEnvGoFunc(pkg, "numcpu", wrap0Arg(numcpu), 0, false)
			rt.SetEnvGoFunc(pkg, "getcpu", wrap0Arg(getcpu), 0, false)
			rt.SetEnvGoFunc(pkg, "setcpu", setcpu, 1, false)
			rt.SetEnvGoFuncContext(pkg, "freemem", wrap0ArgContext(freemem), 0, false)
			rt.SetEnvGoFunc(pkg, "getegid", wrap0Arg(getegid), 0, false)
			rt.SetEnvGoFunc(pkg, "geteuid", wrap0Arg(geteuid), 0, false)
			rt.SetEnvGoFunc(pkg, "getgid", wrap0Arg(getgid), 0, false)
			rt.SetEnvGoFunc(pkg, "getgroups", wrap0Arg(getgroups), 0, false)
			rt.SetEnvGoFunc(pkg, "getuid", wrap0Arg(getuid), 0, false)
			rt.SetEnvGoFunc(pkg, "getpid", wrap0Arg(getpid), 0, false)
			rt.SetEnvGoFunc(pkg, "getppid", wrap0Arg(getppid), 0, false)
			rt.SetEnvGoFunc(pkg, "homedir", wrap0Arg(homedir), 0, false)
			rt.SetEnvGoFunc(pkg, "hostname", wrap0Arg(hostname), 0, false)
			rt.SetEnvGoFunc(pkg, "kill", kill, 1, false)
			rt.SetEnvGoFuncContext(pkg, "loadavg", wrap0ArgContext(loadavg), 0, false)
			rt.SetEnvGoFunc(pkg, "memusage", wrap0Arg(memusage), 0, false)
			rt.SetEnvGoFunc(pkg, "platform", wrap0Arg(platform), 0, false)
			rt.SetEnvGoFunc(pkg, "tmpdir", wrap0Arg(tmpdir), 0, false)
			rt.SetEnvGoFuncContext(pkg, "totalmem", wrap0ArgContext(totalmem), 0, false)
			rt.SetEnvGoFunc(pkg, "uptime", wrap0Arg(uptime), 0, false)
			rt.SetEnvGoFunc(pkg, "userinfo", userinfo, 1, false)
			rt.SetEnvGoFunc(pkg, "networkinterfaces", wrap0Arg(networkinterfaces), 0, false)
			return pkg, nil
		},
	})
}

// arch returns the running program's architecture target: one of "386", "amd64", "arm", "s390x", and so on.
//
// This is determined by the Go compile-time constant GOARCH. See
//		https://golang.org/doc/install/source#environment
// for details.
// Args:
func arch() (rt.Value, error) {
	return rt.String(runtime.GOARCH), nil
}

// chdir changes the current working directory to the named directory. Returns the previous working directory.
// Args: path
func chdir(path string) (rt.Value, error) {
	oldpath, err := os.Getwd()
	if err != nil {
		return nil, err
	} else if err = os.Chdir(path); err != nil {
		return nil, err
	}
	return rt.String(oldpath), nil
}

// cpus returns information about the CPUs available on this system.
//
// CPUs have three levels of counting: sockets, cores, threads. Cores with HyperThreading count as having two threads per core. Sockets often come with many physical CPU cores. For example a single socket board with two cores each with HT will return an array of four elements, each with the "cores" field set to 1.
// Args:
func cpus(ctx context.Context) (rt.Value, error) {
	S, err := cpu.InfoWithContext(ctx)
	if err != nil {
		return nil, err
	}
	// We repackage the data
	res := rt.NewTable()
	for i, s := range S {
		data := rt.NewTable()
		luautil.SetInt(data, "cpu", int(s.CPU))
		luautil.SetString(data, "vendor", s.VendorID)
		luautil.SetString(data, "family", s.Family)
		luautil.SetInt(data, "stepping", int(s.Stepping))
		luautil.SetInt(data, "cores", int(s.Cores))
		luautil.SetString(data, "model", s.ModelName)
		luautil.SetFloat64(data, "speed", s.Mhz)
		luautil.SetInt(data, "cache", int(s.CacheSize))
		flags := rt.NewTable()
		for j, f := range s.Flags {
			flags.Set(rt.Int(j+1), rt.String(f))
		}
		rt.SetEnv(data, "flags", flags)
		res.Set(rt.Int(i+1), data)
	}
	return res, nil
}

// cpuusage returns the amounts of time the CPUs have spent performing different kinds of work. Time units are in USER_HZ or Jiffies (typically hundredths of a second).
//
// The result of a previous call to cpuusage can be passed as the argument to the function, to get a diff reading.
// Args: [diff]
func cpuusage(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Has the user provided a baseline to diff against?
	var origUser, origSystem float64
	if c.NArgs() > 0 && !rt.IsNil(c.Arg(0)) {
		var err error
		origUser, origSystem, err = valueToCPUUsage(c, 0)
		if err != nil {
			return nil, err
		}
	}
	// Record the data for our process
	p, err := process.NewProcess(int32(os.Getpid()))
	if err != nil {
		return nil, convertError(c, err)
	}
	times, err := p.TimesWithContext(ctx)
	if err != nil {
		return nil, convertError(c, err)
	}
	// Package the data up in a table
	t := rt.NewTable()
	luautil.SetFloat64(t, "user", times.User-origUser)
	luautil.SetFloat64(t, "system", times.System-origSystem)
	return c.PushingNext(t), nil
}

// cwd returns a rooted path name corresponding to the current working directory. If the current working directory can be reached via multiple paths (due to symbolic links), cwd may return any one of them.
// Args:
func cwd() (rt.Value, error) {
	path, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	return rt.String(path), nil
}

// env returns all currently set environment variables
// Args:
func env() (rt.Value, error) {
	S := os.Environ()
	t := rt.NewTable()
	for _, s := range S {
		if idx := strings.Index(s, "="); idx != -1 {
			luautil.SetString(t, s[:idx], s[idx+1:])
		}
	}
	return t, nil
}

// setenv sets the value of the named environment variable.
// Args: name value
func setenv(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	name, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	value, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	if err = os.Setenv(string(name), string(value)); err != nil {
		return nil, err
	}
	return nil, nil
}

// getegid returns the numeric effective group id of the caller.
//
// This always returns -1 on Windows
// Args:
func getegid() (rt.Value, error) {
	return rt.Int(os.Getegid()), nil
}

// geteuid returns the numeric effective user id of the caller.
//
// This always returns -1 on Windows
// Args:
func geteuid() (rt.Value, error) {
	return rt.Int(os.Geteuid()), nil
}

// getgid returns the group id of the caller.
//
// This always returns -1 on Windows
// Args:
func getgid() (rt.Value, error) {
	return rt.Int(os.Getgid()), nil
}

// getgroups returns an array of the numeric ids of groups that the caller belongs to.
//
// On Windows this will always raise an error.
// Args:
func getgroups() (rt.Value, error) {
	groups, err := os.Getgroups()
	if err != nil {
		return nil, err
	}
	t := rt.NewTable()
	for i, id := range groups {
		t.Set(rt.Int(i+1), rt.Int(id))
	}
	return t, nil
}

// getuid returns the user id of the caller.
//
// This always returns -1 on Windows
// Args:
func getuid() (rt.Value, error) {
	return rt.Int(os.Getuid()), nil
}

// getpid returns the process id of the caller.
// Args:
func getpid() (rt.Value, error) {
	return rt.Int(os.Getpid()), nil
}

// getppid returns the process id of the caller's parent.
// Args:
func getppid() (rt.Value, error) {
	return rt.Int(os.Getppid()), nil
}

// numcpu returns the number of logical CPUs usable by the current process.
// Args:
func numcpu() (rt.Value, error) {
	return rt.Int(runtime.NumCPU()), nil
}

// getcpu returns the maximum number of CPUs that can be executing simultaneously.
//
// You can control the maximum number of CPUs that can be executing simultaneously via setcpu. The number of logical CPUs on the local machine (which may be different from the number of CPUs that can be executing simultaneously) is equal to numcpu.
// Args:
func getcpu() (rt.Value, error) {
	return rt.Int(runtime.GOMAXPROCS(0)), nil
}

// setcpu sets the maximum number of CPUs that can be executing simultaneously.
//
// The current maximum number of CPUs that can be executing simultaneously can be queried via getcpu, and the number of logical CPUs on the local machine is equal to numcpu.
// Args: n
func setcpu(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	n, err := c.IntArg(0)
	if err != nil {
		return nil, err
	} else if n <= 0 {
		return nil, errors.New("#1 must be a positive integer")
	}
	m := runtime.NumCPU()
	if int64(n) < int64(m) {
		m = int(n)
	}
	runtime.GOMAXPROCS(m)
	return nil, nil
}

// freemem returns current amount of free RAM on this system.
// Args:
func freemem(ctx context.Context) (rt.Value, error) {
	sts, err := mem.VirtualMemoryWithContext(ctx)
	if err != nil {
		return nil, err
	}
	return rt.Int(sts.Free), nil
}

// homedir returns the path to the user's home directory.
// Args:
func homedir() (rt.Value, error) {
	usr, err := user.Current()
	if err != nil {
		return nil, err
	}
	return rt.String(usr.HomeDir), nil
}

// hostname returns the host name reported by the kernel.
// Args:
func hostname() (rt.Value, error) {
	name, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	return rt.String(name), nil
}

// kill kills the process with given process id.
// Args: pid
func kill(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Note the pid
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	pid, err := c.IntArg(0)
	if err != nil {
		return nil, err
	} else if pid < 0 || int64(pid) > maxInt {
		return nil, errors.New("#1 is out-of-range")
	}
	// Attempt to find a matching process
	cmd, err := os.FindProcess(int(pid))
	if err != nil {
		return nil, err
	}
	// Kill the process
	if err = cmd.Kill(); err != nil {
		return nil, err
	}
	return nil, nil
}

// loadavg returns the 1 minute, 5 minute, and 15 minute load averages.
// Args:
func loadavg(ctx context.Context) (rt.Value, error) {
	avg, err := load.AvgWithContext(ctx)
	if err != nil {
		return nil, err
	}
	t := rt.NewTable()
	luautil.SetFloat64(t, "load1", avg.Load1)
	luautil.SetFloat64(t, "load5", avg.Load5)
	luautil.SetFloat64(t, "load15", avg.Load15)
	return t, nil
}

// memusage returns memory allocator statistics.
// Args:
func memusage() (rt.Value, error) {
	m := &runtime.MemStats{}
	runtime.ReadMemStats(m)
	t := rt.NewTable()
	luautil.SetInt64(t, "alloc", int64(m.Alloc))
	luautil.SetInt64(t, "totalalloc", int64(m.TotalAlloc))
	luautil.SetInt64(t, "sys", int64(m.Sys))
	luautil.SetInt64(t, "mallocs", int64(m.Mallocs))
	luautil.SetInt64(t, "frees", int64(m.Frees))
	luautil.SetInt64(t, "lastgc", int64(time.Duration(m.LastGC)/time.Second))
	luautil.SetInt64(t, "gcpause", int64(m.PauseTotalNs))
	luautil.SetInt64(t, "numgc", int64(m.NumGC))
	return t, nil
}

// platform returns he running program's operating system target: one of "darwin", "freebsd", "linux", and so on.
//
// This is determined by the Go compile-time constant GOOS. See
//		https://golang.org/doc/install/source#environment
// for details.
// Args:
func platform() (rt.Value, error) {
	return rt.String(runtime.GOOS), nil
}

// tmpdir returns the default directory to use for temporary files.
//
// The temporary directory is OS-specific. For example, on Unix systems it returns $TMPDIR if non-empty, else /tmp; on Windows it returns the first non-empty value from %TMP%, %TEMP%, %USERPROFILE%, or the Windows directory.
//
// The directory is neither guaranteed to exist nor have accessible permissions.
// Args:
func tmpdir() (rt.Value, error) {
	return rt.String(os.TempDir()), nil
}

// totalmem returns the total amount of RAM on this system.
// Args:
func totalmem(ctx context.Context) (rt.Value, error) {
	sts, err := mem.VirtualMemoryWithContext(ctx)
	if err != nil {
		return nil, err
	}
	return rt.Int(sts.Total), nil
}

// uptime returns the uptime for this system, in seconds.
// Args:
func uptime() (rt.Value, error) {
	n, err := host.Uptime()
	if err != nil {
		return nil, err
	}
	return rt.Int(n), nil
}

// userinfo looks up a user by username. If no username is provided (or the username is nil) then the current user is assumed. This will return nil if the user cannot be found.
// Args: [username]
func userinfo(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the username
	username := ""
	if c.NArgs() != 0 && !rt.IsNil(c.Arg(0)) {
		s, err := c.StringArg(0)
		if err != nil {
			return nil, err
		}
		username = string(s)
	}
	// Fetch the info for the appropriate user
	var err error
	var usr *user.User
	if len(username) == 0 {
		usr, err = user.Current()
	} else {
		usr, err = user.Lookup(username)
	}
	if err != nil {
		return nil, nil
	}
	// Collect together the data
	res := rt.NewTable()
	luautil.SetString(res, "username", usr.Username)
	luautil.SetString(res, "homedir", usr.HomeDir)
	luautil.SetString(res, "name", usr.Name)
	// Convert the uid to an integer
	if n, err := strconv.Atoi(usr.Uid); err == nil {
		luautil.SetInt(res, "uid", n)
	}
	// Convert the gid to an integer
	if n, err := strconv.Atoi(usr.Gid); err == nil {
		luautil.SetInt(res, "gid", n)
	}
	// Return the result
	return c.PushingNext(res), nil
}

// networkinterfaces provides information about those network interfaces that are up and have been assigned a network address.
// Args:
func networkinterfaces() (rt.Value, error) {
	I, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	t := rt.NewTable()
	for _, ifi := range I {
		if (ifi.Flags & net.FlagUp) != 0 {
			addrs, err := ifi.Addrs()
			if err != nil {
				return nil, err
			}
			if len(addrs) != 0 {
				data := rt.NewTable()
				luautil.SetString(data, "mac", ifi.HardwareAddr.String())
				luautil.SetInt(data, "mtu", ifi.MTU)
				luautil.SetBool(data, "broadcast", (ifi.Flags&net.FlagBroadcast) != 0)
				luautil.SetBool(data, "loopback", (ifi.Flags&net.FlagLoopback) != 0)
				luautil.SetBool(data, "pointtopoint", (ifi.Flags&net.FlagPointToPoint) != 0)
				luautil.SetBool(data, "multicast", (ifi.Flags&net.FlagMulticast) != 0)
				ads := rt.NewTable()

				for i, addr := range addrs {
					ad := rt.NewTable()
					luautil.SetString(ad, "network", addr.Network())
					luautil.SetString(ad, "address", addr.String())
					ads.Set(rt.Int(i+1), ad)
				}
				rt.SetEnv(data, "addresses", ads)
				rt.SetEnv(t, ifi.Name, data)
			}
		}
	}
	return t, nil
}
