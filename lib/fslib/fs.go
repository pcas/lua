// Fs describes a Lua view of an fs.Interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fslib

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/lib/cryptolib"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/refcount"
	"context"
	"crypto"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"runtime"
	"strings"
	"sync"
	"time"
)

// cache is a cache of fsd client connections.
var cache = &refcount.Cache{}

// deletionDelay is the delay before removing an unused client connection from the cache.
const deletionDelay = 10 * time.Second

// Fs wraps an fs.Interface.
type Fs struct {
	uri      string              // The connection URI
	fsname   string              // The name of the file store being used
	c        fs.Interface        // The underlying connection
	m        sync.RWMutex        // Controls access to the following
	cwd      string              // The current working directory
	timeout  time.Duration       // The timeout
	ref      *refcount.Reference // The associated reference
	isClosed bool                // Is the connection closed?
}

// readerFunc implements an io.Reader where, prior to each Read, the Interrupt channel is inspected for cancellation.
type readerFunc func([]byte) (int, error)

// writerFunc implements an io.Writer where, prior to each Write, the Interrupt channel is inspected for cancellation.
type writerFunc func([]byte) (int, error)

/////////////////////////////////////////////////////////////////////////
// readerFunc functions
/////////////////////////////////////////////////////////////////////////

// newReaderFunc wraps the given io.Reader r so that, before each call to r.Read, the context is inspected.
func newReaderFunc(ctx context.Context, r io.Reader) io.Reader {
	return readerFunc(func(p []byte) (int, error) {
		select {
		case <-ctx.Done():
			return 0, ctx.Err()
		default:
			return r.Read(p)
		}
	})
}

// Read calls the readerFunc.
func (rf readerFunc) Read(p []byte) (n int, err error) {
	return rf(p)
}

/////////////////////////////////////////////////////////////////////////
// writerFunc functions
/////////////////////////////////////////////////////////////////////////

// newWriterFunc wraps the given io.Writer w so that, before each call to w.Write, the context is inspected.
func newWriterFunc(ctx context.Context, w io.Writer) io.Writer {
	return writerFunc(func(p []byte) (int, error) {
		select {
		case <-ctx.Done():
			return 0, ctx.Err()
		default:
			return w.Write(p)
		}
	})
}

// Write calls the writerFunc.
func (wf writerFunc) Write(p []byte) (n int, err error) {
	return wf(p)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets up the cache of fsd client connections.
func init() {
	// Set the Delete function on the cache
	cache.Delete = func(x interface{}) {
		if c, ok := x.(io.Closer); ok {
			c.Close()
		}
	}
	// Set a Delay on deleting elements from the cache
	cache.Delay = deletionDelay
	// Register a cleanup function to ensure that every element remaining in the
	// cache is closed
	cleanup.Add(func() error {
		cache.Range(func(key interface{}, ref *refcount.Reference) bool {
			if c, ok := ref.Value().(io.Closer); ok {
				c.Close()
			}
			return true
		})
		return nil
	})
}

// fsMethod wraps the GoFunction f with a check that arg(0) is a *Fs.
func fsMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err = FsArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// wrapFsArg wraps the given function, turning it into a GoFunction.
func wrapFsArg(f func(context.Context, *Fs, *rt.GoCont) (rt.Cont, error)) func(context.Context, *rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		conn, err := FsArg(c, 0)
		if err != nil {
			return nil, err
		}
		return f(ctx, conn, c)
	}
}

// wrapPathArg wraps the given function, turning it into a GoFunction.
func wrapPathArg(f func(context.Context, *Fs, string, *rt.GoCont) (rt.Value, error)) func(context.Context, *rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.CheckNArgs(2); err != nil {
			return nil, err
		}
		conn, err := FsArg(c, 0)
		if err != nil {
			return nil, err
		}
		p, err := c.StringArg(1)
		if err != nil {
			return nil, err
		} else if len(p) == 0 {
			return nil, errors.New("#2 must not be the empty string")
		}
		x, e := f(ctx, conn, string(p), c)
		return iobindings.PushingNextResult(c, x, e)
	}
}

// createConnection returns a refcount.CreateFunc that will connection to the file store using the given connection function, with given connection timeout.
func createConnection(r *rt.Runtime, f ConnectFunc, timeout time.Duration) refcount.CreateFunc {
	return func() (interface{}, error) {
		// Create the context
		var cancel context.CancelFunc
		ctx := context.Background()
		if timeout < 0 {
			ctx, cancel = context.WithCancel(ctx)
		} else {
			ctx, cancel = context.WithTimeout(ctx, timeout)
		}
		defer cancel()
		// Link the context to the interrupt channel
		defer contextutil.NewChannelLink(r.Interrupt(), cancel).Stop()
		// Establish the connection
		return f(ctx)
	}
}

// bytesToTable converts the given slice of bytes to a table.
func bytesToTable(S []byte) *rt.Table {
	t := rt.NewTable()
	for i, b := range S {
		t.Set(rt.Int(i+1), rt.Int(b))
	}
	return t
}

/////////////////////////////////////////////////////////////////////////
// Fs functions
/////////////////////////////////////////////////////////////////////////

// connectToFileStore establishes a client connection to a file store using the given connection function. A finaliser is set on the connection to ensure that it is closed.
func connectToFileStore(t *rt.Thread, fileStoreName string, uri string, f ConnectFunc) (*Fs, error) {
	// Recover the timeout value
	timeout := getRegistryData(t).GetTimeout()
	// Fetch (or establish) the connection from the cache
	key := fileStoreName + ":" + uri
	ref, err := cache.LoadOrCreate(key, createConnection(t.Runtime, f, timeout))
	if err != nil {
		return nil, err
	}
	// Create the wrapped-up reference and set the finaliser
	c := &Fs{
		uri:     uri,
		fsname:  fileStoreName,
		cwd:     "/",
		timeout: timeout,
		c:       ref.Value().(fs.Interface),
		ref:     ref,
	}
	runtime.SetFinalizer(c, func(c *Fs) { c.Close() })
	return c, nil
}

// Close prevents any further queries through this connection.
func (c *Fs) Close() error {
	// Sanity check
	if c == nil {
		return nil
	}
	// Acquire a write lock
	c.m.Lock()
	defer c.m.Unlock()
	// Is there anything to do?
	if !c.isClosed {
		c.isClosed = true
		c.ref = nil // Explicitly clear the ref
		runtime.SetFinalizer(c, nil)
	}
	return nil
}

// IsClosed returns true iff the connection is closed.
func (c *Fs) IsClosed() bool {
	if c == nil {
		return true
	}
	c.m.RLock()
	isClosed := c.isClosed
	c.m.RUnlock()
	return isClosed
}

// GetTimeout returns the timeout.
func (c *Fs) GetTimeout() time.Duration {
	if c == nil {
		return 0
	}
	c.m.RLock()
	d := c.timeout
	c.m.RUnlock()
	return d
}

// SetTimeout sets the timeout. A negative timeout is interpreted to mean no timeout.
func (c *Fs) SetTimeout(timeout time.Duration) {
	if c != nil {
		c.m.Lock()
		c.timeout = timeout
		c.m.Unlock()
	}
}

// Getwd returns the current working directory.
func (c *Fs) Getwd() (string, error) {
	// Sanity check
	if c == nil {
		return "", errors.New("uninitialised connection")
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.isClosed {
		return "", errors.New("connection closed")
	}
	// Return the current working directory
	return c.cwd, nil
}

// Chdir sets the current working directory.
func (c *Fs) Chdir(ctx context.Context, p string) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised connection")
	}
	// Acquire a write lock
	c.m.Lock()
	defer c.m.Unlock()
	// Are we closed?
	if c.isClosed {
		return errors.New("connection closed")
	}
	// Ensure that the path is absolute
	if !path.IsAbs(p) {
		p = path.Join(c.cwd, p)
	}
	// Create a timeout context
	if c.timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, c.timeout)
		defer cancel()
	}
	// Check that the path points to a valid directory
	ok, err := fs.IsDir(ctx, c.c, p)
	if err != nil {
		return err
	} else if !ok {
		return fserrors.NotDirectory
	}
	// Looks good -- update the current working directory and return
	c.cwd = p
	return nil
}

// execWithRLock executes the given function f with a read lock. If necessary, the path p will be made absolute (via concatenation with the current working directory) before being passed to f. The context will be modified to timeout after the currently timeout duration before being passed to f.
func (c *Fs) execWithRLock(ctx context.Context, f func(context.Context, string) error, p string) (err error) {
	// Sanity check
	if c == nil {
		err = errors.New("uninitialised connection")
		return
	}
	// Acquire a read lock
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.isClosed {
		err = errors.New("connection closed")
		return
	}
	// If necessary, modify the path using the current working directory
	if !path.IsAbs(p) {
		p = path.Join(c.cwd, p)
	}
	// Create a timeout context
	if c.timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, c.timeout)
		defer cancel()
	}
	// Defer recovering from any panic
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in Fs.execWithRLock function: %v", e)
		}
	}()
	// Execute the function
	err = f(ctx, p)
	return
}

// Upload returns a Writer writing to the path p. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; if a file or directory with path p already exists then an errors.Exist error is returned.
func (c *Fs) Upload(ctx context.Context, p string) (w fs.Writer, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		w, err = c.c.Upload(ctx, p)
		return
	}, p)
	return
}

// Download returns the contents of the file with the path p. It is the caller's responsibility to call Close on the returned io.ReadCloser, otherwise resources may leak.
func (c *Fs) Download(ctx context.Context, p string) (r fs.Reader, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		r, err = c.c.Download(ctx, p)
		return
	}, p)
	return
}

// DownloadMetadata returns the metadata for the file or directory with the path p. If a file or directory with this path does not exist, an errors.NotExist error is returned.
func (c *Fs) DownloadMetadata(ctx context.Context, p string) (md *fs.Metadata, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		md, err = c.c.DownloadMetadata(ctx, p)
		return
	}, p)
	return
}

// Mkdir creates the directory with the path p. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; if a file or directory with that path already exists then an errors.Exist error is returned.
func (c *Fs) Mkdir(ctx context.Context, p string) error {
	return c.execWithRLock(ctx, func(ctx context.Context, p string) error {
		return c.c.Mkdir(ctx, p)
	}, p)
}

// Remove attempts to remove the file or directory with the path p. If the path does not exist, an errors.NotExist error is returned. If the path is a directory and is non-empty or is "/", an errors.DirNotEmpty error is returned.
func (c *Fs) Remove(ctx context.Context, p string) error {
	return c.execWithRLock(ctx, func(ctx context.Context, p string) error {
		return c.c.Remove(ctx, p)
	}, p)
}

// Dir returns an iterator containing metadata for the files and directories within the directory with path p. If the path p does not exist then an errors.NotExist error is returned; if the path p exists but is not a directory then an errors.NotDirectory error will be returned.
func (c *Fs) Dir(ctx context.Context, p string) (itr fs.DirIterator, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		itr, err = c.c.Dir(ctx, p)
		return
	}, p)
	return
}

// IsDir returns true if and only if path p exists in s and is a directory.
func (c *Fs) IsDir(ctx context.Context, p string) (ok bool, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		ok, err = fs.IsDir(ctx, c.c, p)
		return
	}, p)
	return
}

// IsDirEmpty returns true if and only if path p exists in s and is an empty directory. If the path is not a directory, returns errors.NotDirectory.
func (c *Fs) IsDirEmpty(ctx context.Context, p string) (ok bool, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		ok, err = fs.IsDirEmpty(ctx, c.c, p)
		return
	}, p)
	return
}

// MkdirRecursive creates the directory with the given path p, along with any intermediate directories as necessary. An errors.Exist error is returned if a file or directory with that path already exists, or if a file already exists with an intermediate path.
func (c *Fs) MkdirRecursive(ctx context.Context, p string) error {
	return c.execWithRLock(ctx, func(ctx context.Context, p string) error {
		return fs.MkdirRecursive(ctx, c.c, p)
	}, p)
}

// Rmdir removes the path p from s, if p is an empty directory. If the path is not a directory, returns errors.NotDirectory; if the directory is not empty or is "/", returns errors.DirNotEmpty.
func (c *Fs) Rmdir(ctx context.Context, p string) error {
	return c.execWithRLock(ctx, func(ctx context.Context, p string) error {
		return fs.Rmdir(ctx, c.c, p)
	}, p)
}

// RemoveAll removes path p from s if p is a file, and removes p and all its contents from s if p is a directory. If the path does not exist, RemoveAll returns nil (no error).
func (c *Fs) RemoveAll(ctx context.Context, p string) error {
	return c.execWithRLock(ctx, func(ctx context.Context, p string) error {
		return fs.RemoveAll(ctx, c.c, p)
	}, p)
}

// CopyAll recursively copies the file or directory structure rooted at path src to path dst. If src does not exist, then errors.NotExist is returned. If dst's parent directory does not exist, an errors.NotExist error is returned; an errors.Exist error is returned if a file or directory with path dst already exists.
func (c *Fs) CopyAll(ctx context.Context, src string, dst string) error {
	return c.execWithRLock(ctx, func(ctx context.Context, src string) error {
		if !path.IsAbs(dst) {
			dst = path.Join(c.cwd, dst)
		}
		return fs.CopyAll(ctx, c.c, src, dst)
	}, src)
}

// Copy copies the contents of the file at src in s to dst in s. If src does not exist or is not a file, then errors.NotExist is returned. If dst's parent directory does not exist, an errors.NotExist error is returned; an errors.Exist error is returned if a file or directory with path dst already exists.
func (c *Fs) Copy(ctx context.Context, src string, dst string) (n int64, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, src string) (err error) {
		if !path.IsAbs(dst) {
			dst = path.Join(c.cwd, dst)
		}
		n, err = fs.Copy(ctx, c.c, src, dst)
		return
	}, src)
	return
}

// Exists returns true if and only if path p exists in s.
func (c *Fs) Exists(ctx context.Context, p string) (ok bool, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		ok, err = fs.Exists(ctx, c.c, p)
		return
	}, p)
	return
}

// IsFile returns true if and only if path p exists in s and is a file.
func (c *Fs) IsFile(ctx context.Context, p string) (ok bool, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		ok, err = fs.IsFile(ctx, c.c, p)
		return
	}, p)
	return
}

// RemoveFile attempts to delete the file with path p in s. If p is not a file, then the error ErrNotFile will be returned.
func (c *Fs) RemoveFile(ctx context.Context, p string) error {
	return c.execWithRLock(ctx, func(ctx context.Context, p string) error {
		return fs.RemoveFile(ctx, c.c, p)
	}, p)
}

// ReplaceFile attempts to create or replace the file with path p in s. If p's parent directory does not exist, an errors.NotExist error is returned; if the parent exists but is not a directory then an errors.NotDirectory error is returned; if p exists but is not a file, then the error errors.NotFile will be returned. It is the caller's responsibility to call Close on the returned Writer, otherwise resources may leak.
func (c *Fs) ReplaceFile(ctx context.Context, p string) (w fs.Writer, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		w, err = fs.ReplaceFile(ctx, c.c, p)
		return
	}, p)
	return
}

// Hash returns the hash of the file with path p on s, using the hash h. If the hash is not available (as determined by crypto.Hash.Available) then an errors.HashNotAvailable error will be returned. If the path does not exist, or is a directory, an errors.NotExist error is returned.
func (c *Fs) Hash(ctx context.Context, p string, h crypto.Hash) (b []byte, err error) {
	err = c.execWithRLock(ctx, func(ctx context.Context, p string) (err error) {
		b, err = fs.Hash(ctx, c.c, p, h)
		return
	}, p)
	return
}

// String returns a string description of the connection.
func (c *Fs) String() string {
	if c == nil {
		return "fs(nil)"
	}
	return "fs." + c.fsname + "(\"" + c.uri + "\")"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createFsMetatable creates the metatable for a connection.
func createFsMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "settimeout", fsMethod(iobindings.SetTimeout), 2, false)
	rt.SetEnvGoFunc(methods, "gettimeout", fsMethod(iobindings.GetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "close", fsMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", fsMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFuncContext(methods, "chdir", wrapPathArg(chdir), 2, false)
	rt.SetEnvGoFunc(methods, "cwd", cwd, 1, false)
	rt.SetEnvGoFuncContext(methods, "uploadfile", wrapFsArg(uploadfile), 3, false)
	rt.SetEnvGoFuncContext(methods, "downloadfile", wrapFsArg(downloadfile), 3, false)
	rt.SetEnvGoFuncContext(methods, "open", open, 3, false)
	rt.SetEnvGoFuncContext(methods, "rmdir", wrapPathArg(rmdir), 2, false)
	rt.SetEnvGoFuncContext(methods, "rmdirall", wrapPathArg(rmdirall), 2, false)
	rt.SetEnvGoFuncContext(methods, "mkdir", wrapPathArg(mkdir), 2, false)
	rt.SetEnvGoFuncContext(methods, "mkdirall", wrapPathArg(mkdirall), 2, false)
	rt.SetEnvGoFuncContext(methods, "readdir", wrapFsArg(readdir), 2, false)
	rt.SetEnvGoFuncContext(methods, "readfile", readfile, 2, false)
	// TODO remove file
	// TODO change default upload and open(_,"w") behaviour to clobber files that exist
	rt.SetEnvGoFuncContext(methods, "writefile", wrapPathArg(writefile), 2, true)
	rt.SetEnvGoFuncContext(methods, "exists", wrapPathArg(exists), 2, false)
	rt.SetEnvGoFuncContext(methods, "isdir", wrapPathArg(isdir), 2, false)
	rt.SetEnvGoFuncContext(methods, "isfile", wrapPathArg(isfile), 2, false)
	rt.SetEnvGoFuncContext(methods, "emptydir", wrapFsArg(emptydir), 2, false)
	rt.SetEnvGoFuncContext(methods, "hash", wrapFsArg(hash), 4, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", fsMethod(iobindings.ToString), 1, false)
	return meta
}

// chdir changes the current working directory to the named directory. Returns the previous working directory.
// Args: conn path
func chdir(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	old, err := conn.Getwd()
	if err != nil {
		return nil, err
	}
	return rt.String(old), conn.Chdir(ctx, p)
}

// cwd returns a rooted path name corresponding to the current working directory. If the current working directory can be reached via multiple paths (due to symbolic links), cwd may return any one of them.
// Args: conn
func cwd(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	conn, err := FsArg(c, 0)
	if err != nil {
		return nil, err
	}
	p, e := conn.Getwd()
	return iobindings.PushingNextResult(c, rt.String(p), e)
}

// uploadfile uploads the file with path src to the path dst. If file already exists at path dst it is replaced.
// Args: conn src dst
func uploadfile(ctx context.Context, conn *Fs, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	// Recover the src and dst paths
	src, err := c.StringArg(1)
	if err != nil {
		return nil, err
	} else if len(src) == 0 {
		return nil, errors.New("#2 must not be the empty string")
	}
	dst, err := c.StringArg(2)
	if err != nil {
		return nil, err
	} else if len(dst) == 0 {
		return nil, errors.New("#3 must not be the empty string")
	}
	// Open the source file for reading
	r, err := os.Open(string(src))
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer r.Close()
	// Create a writer writing to dst
	w, err := fs.ReplaceFile(ctx, conn, string(dst))
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer w.Close()
	// Copy src to dst
	_, err = io.Copy(newWriterFunc(ctx, w), r)
	return iobindings.PushingError(c, err)
}

// downloadfile downloads the file with path src to the path dst.
// Args: conn src dst
func downloadfile(ctx context.Context, conn *Fs, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	// Recover the src and dst paths
	src, err := c.StringArg(1)
	if err != nil {
		return nil, err
	} else if len(src) == 0 {
		return nil, errors.New("#2 must not be the empty string")
	}
	dst, err := c.StringArg(2)
	if err != nil {
		return nil, err
	} else if len(dst) == 0 {
		return nil, errors.New("#3 must not be the empty string")
	}
	// Open the source file
	r, err := conn.Download(ctx, string(src))
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer r.Close()
	// Open the destination file for writing
	w, err := os.Create(string(dst))
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer w.Close()
	// Download the data
	_, err = io.Copy(w, newReaderFunc(ctx, r))
	return iobindings.PushingError(c, err)
}

// open opens the contents of the given path in the given mode. It is the caller's responsibility to close the returned file, otherwise resources will leak.
//
// The mode (which is optional) must be one of:
//	'r'	read only (default);
//	'w'	write only (the file is created if it doesn't exist, otherwise it is truncated).
// Args: conn path [mode]
func open(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the connection and path
	conn, err := FsArg(c, 0)
	if err != nil {
		return nil, err
	}
	p, err := c.StringArg(1)
	if err != nil {
		return nil, err
	} else if len(p) == 0 {
		return nil, errors.New("#2 must not be the empty string")
	}
	// Recover the mode
	mode := "r"
	if c.NArgs() > 2 && !rt.IsNil(c.Arg(2)) {
		s, err := c.StringArg(2)
		if err != nil {
			return nil, err
		}
		mode = strings.TrimSuffix(string(s), "b")
	}
	// How we proceed depends on the mode
	switch mode {
	case "r":
		// Download the data
		r, err := conn.Download(ctx, string(p))
		if err != nil {
			return iobindings.PushingError(c, err)
		}
		// Convert the reader to a value and return
		f := newReader(r, conn.GetTimeout())
		return c.PushingNext(rt.NewUserData(f, getRegistryData(t).readerMt)), nil
	case "w":
		// Create the writer
		w, err := fs.ReplaceFile(ctx, conn, string(p))
		if err != nil {
			return iobindings.PushingError(c, err)
		}
		// Convert the writer to a value and return
		f := newWriter(w, conn.GetTimeout())
		return c.PushingNext(rt.NewUserData(f, getRegistryData(t).writerMt)), nil
	default:
		return nil, errors.New("invalid mode")
	}
}

// rmdir removes (deletes) the directory at the given path, provided that it is empty. If you need to remove a non-empty directory, see rmdirall.
// Args: conn path
func rmdir(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	return nil, fs.Rmdir(ctx, conn, p)
}

// rmdirall removes (deletes) the directory at the given path, along with any files or directories that it contains.
//
// Warning: This will recursively delete the contents of the directory. Use with care.
// Args: conn path
func rmdirall(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	ok, err := fs.IsDir(ctx, conn, p)
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, fserrors.NotDirectory
	}
	return nil, fs.RemoveAll(ctx, conn, p)
}

// mkdir creates the directory with the given path using the given connection.
// Args: conn path
func mkdir(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	return nil, conn.Mkdir(ctx, p)
}

// mkdirall creates a new directory with the specified path, creating any intermediate directories as required.
// Args: conn path
func mkdirall(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	return nil, fs.MkdirRecursive(ctx, conn, p)
}

// readdir reads and returns an array of names from the directory with given path, or the current working directory if no path is specified.
// Args: conn [path]
func readdir(ctx context.Context, conn *Fs, c *rt.GoCont) (rt.Cont, error) {
	// Recover the path
	var p string
	if c.NArgs() > 1 && !rt.IsNil(c.Arg(1)) {
		s, err := c.StringArg(1)
		if err != nil {
			return nil, err
		} else if len(s) == 0 {
			return nil, errors.New("#2 must not be the empty string")
		}
		p = string(s)
	} else {
		var err error
		if p, err = conn.Getwd(); err != nil {
			return nil, err
		}
	}
	// Open the path
	itr, err := conn.Dir(ctx, p)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer itr.Close()
	// Read the directory listing into a Lua table
	t := rt.NewTable()
	i := 1
	md := &fs.Metadata{}
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		if err = itr.Scan(md); err == nil {
			t.Set(rt.Int(i), rt.String(path.Base(md.Path)))
			ok, err = itr.NextContext(ctx)
			i++
		}
	}
	return iobindings.PushingNextResult(c, t, err)
}

// readfile returns the contents of the file with given path.
//
// Warning: Calling readfile on a large amount of data can potentially exhaust the available memory.
// Args: conn path
func readfile(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the connection and path
	conn, err := FsArg(c, 0)
	if err != nil {
		return nil, err
	}
	p, err := c.StringArg(1)
	if err != nil {
		return nil, err
	} else if len(p) == 0 {
		return nil, errors.New("#2 must not be the empty string")
	}
	// Open the file
	r, err := conn.Download(ctx, string(p))
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer r.Close()
	// Read the contents
	s, err := iobindings.ReadAll(newReaderFunc(ctx, r), t)
	return iobindings.PushingNextResult(c, rt.String(s), err)
}

// writefile writes to the file with given path. If the file does not exist, writefile creates it; otherwise writefile truncates it before writing.
// Args: conn path [val1 [... valn]]
func writefile(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	// Create the writer for uploading
	w, err := conn.Upload(ctx, p)
	if err != nil {
		return nil, err
	}
	defer w.Close()
	// Write the values to the buffer and return
	if err := iobindings.WriteValues(newWriterFunc(ctx, w), c.Etc()); err != nil {
		return nil, err
	}
	return rt.Bool(true), nil
}

// exists returns true if a file or directory exists with the given path.
// Args: conn path
func exists(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	ok, err := fs.Exists(ctx, conn, p)
	return rt.Bool(ok), err
}

// isdir returns true if a directory exists with the given path.
// Args: conn path
func isdir(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	ok, err := fs.IsDir(ctx, conn, p)
	return rt.Bool(ok), err
}

// isfile returns true if a file exists with the given path.
// Args: conn path
func isfile(ctx context.Context, conn *Fs, p string, c *rt.GoCont) (rt.Value, error) {
	ok, err := fs.IsFile(ctx, conn, p)
	return rt.Bool(ok), err
}

// emptydir returns true if a directory exists with the given path and is empty. If no path is given then the current working directory is used.
// Args: conn [path]
func emptydir(ctx context.Context, conn *Fs, c *rt.GoCont) (rt.Cont, error) {
	// Recover the path
	var p string
	if c.NArgs() > 1 && !rt.IsNil(c.Arg(1)) {
		s, err := c.StringArg(1)
		if err != nil {
			return nil, err
		} else if len(s) == 0 {
			return nil, errors.New("#2 must not be the empty string")
		}
		p = string(s)
	} else {
		var err error
		if p, err = conn.Getwd(); err != nil {
			return nil, err
		}
	}
	// Check if this is an empty directory
	ok, err := fs.IsDirEmpty(ctx, conn, p)
	return iobindings.PushingNextResult(c, rt.Bool(ok), err)
}

// hash returns the hash of the file with the given path.
// Args: conn path dtype [raw]
func hash(ctx context.Context, conn *Fs, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	// Recover the path and dtype
	p, err := c.StringArg(1)
	if err != nil {
		return nil, err
	} else if len(p) == 0 {
		return nil, errors.New("#2 must not be the empty string")
	}
	dtype, err := c.StringArg(2)
	if err != nil {
		return nil, err
	}
	// Recover the hash type
	h, ok := cryptolib.HashMap[string(dtype)]
	if !ok {
		return nil, errors.New("#3 is not a valid dtype")
	}
	// Calculate the hash
	b, err := fs.Hash(ctx, conn, string(p), h)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	// Return the value
	raw := c.NArgs() > 3 && rt.Truth(c.Arg(3))
	if raw {
		return c.PushingNext(bytesToTable(b)), nil
	}
	return c.PushingNext(rt.String(fmt.Sprintf("%x", b))), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FsArg turns a continuation argument into an *Fs.
func FsArg(c *rt.GoCont, n int) (*Fs, error) {
	conn, ok := ValueToFs(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be an fs connection", n+1)
	}
	return conn, nil
}

// ValueToFs turns a lua value to an *Fs, if possible.
func ValueToFs(v rt.Value) (*Fs, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	conn, ok := u.Value().(*Fs)
	return conn, ok
}
