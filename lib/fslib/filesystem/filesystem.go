// Filesystem contains Lua helpers for the fs/filesystem file store.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fsd

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/filesystem"
	"bitbucket.org/pcas/golua/lib/packagelib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/lib/fslib"
	"bitbucket.org/pcas/lua/luautil"
	"context"
	"fmt"
)

// fileStoreName is the name of the file store.
const fileStoreName = "filesystem"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// defaultDir returns the default directory as specified by fs.<fileStoreName>.defaultdir.
func defaultDir(t *rt.Thread) (string, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("fs"))
	if err != nil {
		return "", err
	}
	// Recover the default dir
	dir, wasSet, err := luautil.GetString(pkg, fileStoreName, "defaultdir")
	if err != nil {
		return "", err
	} else if !wasSet {
		return "", fmt.Errorf("'%s.defaultdir' is not a string", fileStoreName)
	}
	return dir, nil
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers us with the fs package.
func init() {
	// Register our package table with the fs package
	fslib.Register(fileStoreName, func() *rt.Table {
		// Create the package table
		pkg := rt.NewTable()
		rt.SetEnv(pkg, "defaultdir", rt.String("/"))
		rt.SetEnvGoFunc(pkg, "connect", fslib.WrapConnectFunc(fileStoreName, connect), 1, false)
		// Return the table
		return pkg
	})
}

// connect returns a new connection to the file system. The caller must call close on the connection when finished. If the base directory is not provided then the default directory is used.
//
// Args: [config]
func connect(t *rt.Thread, c *rt.GoCont) (string, fslib.ConnectFunc, error) {
	// Recover the base directory
	var dir string
	if c.NArgs() > 0 {
		v, err := c.StringArg(0)
		if err != nil {
			return "", nil, err
		}
		dir = string(v)
	} else {
		var err error
		dir, err = defaultDir(t)
		if err != nil {
			return "", nil, err
		}
	}
	// Return the URI and connection function
	return dir, func(_ context.Context) (fs.Interface, error) {
		return filesystem.New(dir)
	}, nil
}
