// Fsd contains Lua helpers for the fs/fsd file store.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fsd

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/fsd"
	"bitbucket.org/pcas/golua/lib/packagelib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/lib/fslib"
	"bitbucket.org/pcas/lua/luautil"
	"bitbucket.org/pcastools/address"
	"context"
)

// fileStoreName is the name of the file store.
const fileStoreName = "fsd"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// configToTable returns a table representing the given client configuration.
func configToTable(cfg *fsd.ClientConfig) *rt.Table {
	t := rt.NewTable()
	luautil.SetString(t, "address", cfg.URI())
	luautil.SetString(t, "sslcert", string(cfg.SSLCert))
	return t
}

// tableToConfig returns a copy of the given client configuration modified with the values in t.
func tableToConfig(cfg *fsd.ClientConfig, t *rt.Table) (*fsd.ClientConfig, error) {
	// Move to a copy
	if cfg == nil {
		cfg = fsd.DefaultConfig()
	} else {
		cfg = cfg.Copy()
	}
	// Modify the values
	if s, ok, err := luautil.GetString(t, "address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	if s, ok, err := luautil.GetString(t, "sslcert"); ok {
		if err != nil {
			return nil, err
		}
		cfg.SSLCert = []byte(s)
	}
	// Return the config settings
	return cfg, nil
}

// defaultConfig returns the default config as specified by fs.<fileStoreName>.defaults.
func defaultConfig(t *rt.Thread) (*fsd.ClientConfig, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("fs"))
	if err != nil {
		return nil, err
	}
	// Recover the defaults table
	defaults, err := luautil.Subtable(pkg, fileStoreName, "defaults")
	if err != nil {
		return nil, err
	}
	// Convert the values
	return tableToConfig(nil, defaults)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers us with the fs package.
func init() {
	// Register our package table with the fs package
	fslib.Register(fileStoreName, func() *rt.Table {
		// Create the package table
		pkg := rt.NewTable()
		rt.SetEnv(pkg, "defaults", configToTable(fsd.DefaultConfig()))
		rt.SetEnvGoFunc(pkg, "connect", fslib.WrapConnectFunc(fileStoreName, connect), 1, false)
		// Return the table
		return pkg
	})
}

// connect returns a new connection to the fsd server. The caller must call close on the connection when finished. If config is nil then the default configuration settings will be used. The optional table config can be used to modify these defaults; in particular, config can be used to specify values for:
//	'address'	a string specifying the address to connect to;
//	'sslcert'	the SSL certificate (if any).
//
// Args: [config]
func connect(t *rt.Thread, c *rt.GoCont) (string, fslib.ConnectFunc, error) {
	// Read the config
	cfg, err := defaultConfig(t)
	if err != nil {
		return "", nil, err
	}
	if c.NArgs() > 0 {
		v, err := c.TableArg(0)
		if err != nil {
			return "", nil, err
		}
		if cfg, err = tableToConfig(cfg, v); err != nil {
			return "", nil, err
		}
	}
	// Validate the settings
	if err = cfg.Validate(); err != nil {
		return "", nil, err
	}
	// Return the URI and connection function
	return cfg.URI(), func(ctx context.Context) (fs.Interface, error) {
		return fsd.NewClient(ctx, cfg)
	}, nil
}
