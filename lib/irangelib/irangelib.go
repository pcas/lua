// Irangelib contains Lua glue for the irange package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irangelib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
)

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	// Register this module
	module.Register(module.Info{
		Name:        "fs",
		Description: "Provides pcas irange access.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Create the package table
			pkg := rt.NewTable()
			return pkg, nil
		},
	})
}
