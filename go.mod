module bitbucket.org/pcas/lua

go 1.15

require (
	bitbucket.org/pcas/fs v0.1.10
	bitbucket.org/pcas/golua v0.1.5
	bitbucket.org/pcas/keyvalue v0.1.27
	bitbucket.org/pcas/logger v0.1.28
	bitbucket.org/pcas/metrics v0.1.23
	bitbucket.org/pcas/sslflag v0.0.12
	bitbucket.org/pcastools/address v0.1.3
	bitbucket.org/pcastools/bytesbuffer v1.0.2
	bitbucket.org/pcastools/cleanup v1.0.3
	bitbucket.org/pcastools/compress v1.0.2
	bitbucket.org/pcastools/contextutil v1.0.2
	bitbucket.org/pcastools/convert v1.0.4
	bitbucket.org/pcastools/file v1.0.2
	bitbucket.org/pcastools/flag v0.0.16
	bitbucket.org/pcastools/log v1.0.3
	bitbucket.org/pcastools/refcount v1.0.3
	bitbucket.org/pcastools/stringsbuilder v1.0.2
	bitbucket.org/pcastools/ulid v0.1.4
	bitbucket.org/pcastools/version v0.0.4
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/chzyer/test v0.0.0-20210722231415-061457976a23 // indirect
	github.com/fatih/color v1.13.0
	github.com/juju/ansiterm v0.0.0-20210929141451-8b71cc96ebdc
	github.com/lib/pq v1.10.4 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1
	github.com/pkg/errors v0.9.1
	github.com/shirou/gopsutil v3.21.10+incompatible
	github.com/virtao/GoEndian v0.0.0-20140331034613-586fa83c856a
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa // indirect
	golang.org/x/net v0.0.0-20211109214657-ef0fda0de508 // indirect
	golang.org/x/sys v0.0.0-20211110154304-99a53858aa08 // indirect
	mvdan.cc/xurls/v2 v2.3.0
)
