// Lua is the pcas Lua runtime.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lua

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	_ "bitbucket.org/pcas/lua/lib"
	"io"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new Lua runtime, with the pcas modules installed, and with the given stdout.
func New(stdout io.Writer) (*rt.Runtime, error) {
	r := rt.New(stdout)
	if err := module.Init(r); err != nil {
		return nil, err
	}
	return r, nil
}
