// Printer provides a printer for the REPL.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package repl

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/repl/internal/printer"
	"bitbucket.org/pcastools/stringsbuilder"
	"errors"
	"fmt"
	"github.com/fatih/color"
	"github.com/mitchellh/go-wordwrap"
	"io"
	"mvdan.cc/xurls/v2"
	"strings"
)

// Printer provides a way to print REPL values to a io.Writer.
type Printer struct {
	p *printer.Printer // The underlying printer
}

// urlRegexp is a regular expression for matching against URLs
var urlRegexp = xurls.Strict()

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// flushLeft ensures that the given string is flushed left.
func flushLeft(s string) string {
	// Split the strings on line-breaks
	S := strings.Split(s, "\n")
	// Look for the minimum-length of space starting line. A blank line counts
	// as an infinite amount of space. A tab doesn't count as space.
	n := -1
	for i, line := range S {
		if l := strings.TrimLeft(line, " "); len(l) == 0 {
			S[i] = ""
		} else if k := len(line) - len(l); n == -1 || k < n {
			n = k
		}
	}
	// Now we trim n characters from the start of each line
	if n > 0 {
		for i, line := range S {
			if len(line) >= n {
				S[i] = line[n:]
			}
		}
	}
	// Return the trimmed lines
	return strings.Join(S, "\n")
}

/////////////////////////////////////////////////////////////////////////
// Printer functions
/////////////////////////////////////////////////////////////////////////

// NewPrinter returns a new printer for the given vm.
func NewPrinter(vm *rt.Runtime) *Printer {
	return &Printer{p: printer.New(vm)}
}

// getScreenWidth returns the target display width (in characters). This will be used for line-wrapping. A width <= 0 indicates an "infinitely" wide target display.
func (p *Printer) getScreenWidth() int {
	if p == nil {
		return 0
	}
	return p.p.GetScreenWidth()
}

// SetScreenWidth sets the target display width (in characters). This will be used for line-wrapping. A width <= 0 indicates an "infinitely" wide target display.
func (p *Printer) SetScreenWidth(w int) {
	if p != nil {
		p.p.SetScreenWidth(w)
	}
}

// SetColor enables (true) or disables (false) colourised output.
func (p *Printer) SetColor(enable bool) {
	if p != nil {
		p.p.SetColor(enable)
	}
}

// GetColor returns the colour for the given class.
func (p *Printer) getColor(t printer.Color) *color.Color {
	if p == nil {
		return printer.NoColor
	}
	return p.p.GetColor(t)
}

// Bold converts the given string to bold text, if colours are enabled. Otherwise the original string is returned.
func (p *Printer) Bold(s string) string {
	return p.getColor(printer.ColorBold).Sprint(s)
}

// Underline underlines the given string, if colours are enabled. Otherwise the original string is returned.
func (p *Printer) Underline(s string) string {
	return p.getColor(printer.ColorUnderline).Sprint(s)
}

// WrapString wraps the given string to the current screen width.
func (p *Printer) WrapString(s string) string {
	if width := p.getScreenWidth(); width > 0 {
		s = wordwrap.WrapString(s, uint(width))
	}
	return s
}

// FprintValue prints the values to w. It returns the number of bytes written and any write error encountered.
func (p *Printer) FprintValue(w io.Writer, vs ...rt.Value) (int, error) {
	if p == nil {
		return 0, errors.New("illegal nil printer")
	}
	items := make([]printer.Item, 0, len(vs))
	for _, v := range vs {
		items = append(items, p.p.NewItem(v))
	}
	inline := true
	for _, x := range items {
		if !x.IsInline() {
			inline = false
		}
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	if inline {
		for i, x := range items {
			if i != 0 {
				b.WriteByte('\t')
			}
			if _, err := p.p.WriteTo(b, x); err != nil {
				return 0, err
			}
		}
	} else {
		for i, x := range items {
			if i != 0 {
				b.WriteByte('\n')
			}
			if _, err := p.p.WriteTo(b, x); err != nil {
				return 0, err
			}
		}
	}
	return w.Write([]byte(b.String()))
}

// FprintValueInline prints the values to w. It returns the number of bytes written and any write error encountered. The values will be printed inline.
func (p *Printer) FprintValueInline(w io.Writer, vs ...rt.Value) (int, error) {
	if p == nil {
		return 0, errors.New("illegal nil printer")
	}
	items := make([]printer.Item, 0, len(vs))
	for _, v := range vs {
		items = append(items, p.p.NewItem(v))
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	for i, x := range items {
		x.ForceInline()
		if i != 0 {
			b.WriteByte('\t')
		}
		if _, err := p.p.WriteTo(b, x); err != nil {
			return 0, err
		}
	}
	return w.Write([]byte(b.String()))
}

// FprintError prints the error err to w. It returns the number of bytes written and any write error encountered.
func (p *Printer) FprintError(w io.Writer, err error) (int, error) {
	// Create the error message
	text := rt.Traceback(err)
	// Wrap the text to the screen width
	text = p.WrapString(strings.TrimSpace(text))
	// Write the message
	return p.getColor(printer.ColorError).Fprint(w, text)
}

// FprintHelp prints the help message to w. It returns the number of bytes written and any write error encountered.
func (p *Printer) FprintHelp(w io.Writer, title string, text string) (n int, err error) {
	// Output the title
	bold := p.getColor(printer.ColorBold)
	if n, err = bold.Fprintf(w, "%s", title); err != nil {
		return
	}
	// If there isn't any text, return
	if len(text) == 0 {
		return
	}
	// Rule off the title
	var m int
	m, err = fmt.Fprint(w, "\n")
	n += m
	if err != nil {
		return
	}
	m, err = fmt.Fprintln(w, strings.Repeat("-", len(title)))
	n += m
	if err != nil {
		return
	}
	// Ensure that the text is flushed left and wrapped to the screen width
	text = p.WrapString(flushLeft(text))
	// Convert special strings to bold
	for _, s := range []string{
		"Usage",
		"Example",
		"Examples",
		"Warning",
		"Warnings",
		"Note",
		"Notes",
	} {
		text = strings.Replace(text, s+"\n", bold.Sprint(s)+"\n", -1)
	}
	text = strings.Replace(text, "// Output:", "// "+bold.Sprint("Output:"), -1)
	// Underling any URLs
	ul := p.getColor(printer.ColorUnderline)
	for _, url := range urlRegexp.FindAllString(text, -1) {
		text = strings.Replace(text, url, ul.Sprint(url), 1)
	}
	// Output the text
	m, err = fmt.Fprint(w, text)
	n += m
	return
}
